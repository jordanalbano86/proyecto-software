
    function deleteDocument(id) {
        if (confirm('¿Estás seguro de eliminar este documento?')) {
            fetch(`<?= base_url('document_types/delete') ?>?id=${id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(response => {
                if (response.ok) {
                    // Realizar acciones después de la eliminación, como recargar la página o actualizar la lista
                    location.reload(); // Por ejemplo, recargar la página
                } else {
                    alert('No se pudo eliminar el documento');
                }
            })
            .catch(error => {
                console.error('Error al eliminar el documento:', error);
            });
        }
    }
