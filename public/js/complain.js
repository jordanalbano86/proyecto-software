function initComplain(id) {
    fetch('../complain/init_complain', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ id: id }),
    })

    .then(response => {
        if (!response.ok) {
            throw new Error('Error al iniciar la denuncia');
        }
        return response.json();
    })
    .then(data => {
        if (data.success) {
            alert('Denuncia iniciada exitosamente');
            location.reload();
        } else {
            console.log(data);
            alert('Error al iniciar la denuncia');
        }
    })
    .catch(error => {
        console.error(error);
        console.error('Error de conexión', error);
        alert('Error de conexión');
    });
}

function termComplain(id) {
    fetch('../complain/term_complain', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ id: id }),
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Error al finalizar la denuncia');
        }
        return response.json();
    })
    .then(data => {
        if (data.success) {
            alert('Denuncia finalizada exitosamente');
            location.reload();
        } else {
            console.log(data);
            alert('Error al finalizar la denuncia');
        }
    })
    .catch(error => {
        console.error('Error de conexión', error);
        alert(error);
    });
}

