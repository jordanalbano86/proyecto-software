function load_citys() {

    // Obtener el valor seleccionado en el dropdown de provincias
    var selectedState = document.getElementById("states").value;
    // Realizar una solicitud AJAX para obtener las ciudades correspondientes
    // Supongamos que estás utilizando la librería Fetch para hacer la solicitud

    fetch('../City_controller/get_cities?id=' + selectedState, {
    method: 'GET'
    })
    .then(response => {
      if (response.status === 200) {
       return response.json(); // Convierte la respuesta a JSON si es un 200 OK
   } else {
       return []; // Devuelve un array vacío si la respuesta no es un 200 OK
   }
    })
    .then(data => {
         console.log(data);
        // Actualizar el dropdown de ciudades con los datos recibidos
        var cityDropdown = document.getElementById("city");
        cityDropdown.innerHTML = ''; // Limpiar el dropdown
        data.forEach(function(city) {
            var option = document.createElement("option");
            option.value = city.id;
            option.text = city.name;
            cityDropdown.appendChild(option);
        });
    })
    .catch(error => {
         console.error('Error en la solicitud:', error);
         // Agrega un console.log para depurar
         console.log(error.message);
     });

}
