function deleteRecord(documentId) {
        if (confirm("¿Estás seguro de que deseas eliminar este documento?")) {
            fetch('../document_steps/delete?id='+documentId, {
                method: 'DELETE',

                // Puedes incluir más opciones como el cuerpo de la solicitud (si es necesario).
            })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Error al eliminar el documento');
                }
                // Puedes redirigir o realizar otras acciones después de eliminar exitosamente.
                window.location.reload(); // Recargar la página como ejemplo.
            })
            .catch(error => {
                console.error('Error:', error);
                // Manejar errores aquí, mostrar un mensaje, etc.
            });
        }
    }

    function editRecord(id,docu) {
            if (confirm("¿Estás seguro de que desea ver los pasos de recuperacion de este documento?")) {
              console.log(docu);
              window.location.href = '../document_steps/form_update?id=' + id + '&document=' + docu;
            }
        }
