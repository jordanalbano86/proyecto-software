// Configuración de paginación
var itemsPerPage = 5;
var currentPage = 1;



function displayData() {
    var startIndex = (currentPage - 1) * itemsPerPage;
    var endIndex = startIndex + itemsPerPage;
    var tableBody = document.getElementById("table-body");
    tableBody.innerHTML = '';

    for (var i = startIndex; i < endIndex && i < entityData.length; i++) {
        var row = entityData[i];
        var rowHtml = '<tr>';
        rowHtml += '<td>' + row.id + '</td>';
        rowHtml += '<td>' + row.name + '</td>';
        rowHtml += '<td>' + row.email + '</td>';
        rowHtml += '<td>' + row.number +' '+row.street + '</td>';
        rowHtml += '<td>' + row.province + '</td>';
        rowHtml += '<td>' + row.city + '</td>';
        rowHtml += '<td>' + row.phone + '</td>';
        rowHtml += '<td><button class="btn btn-info" onclick="viewDetail(' + row.id + ')"><span id="mod" class="material-icons"> search </span></button> ';
        rowHtml += '<button class="btn btn-secondary" onclick="editRecord(' + row.id + ')"><span id="mod" class="material-icons"> edit </span></button> ';
        rowHtml += '<button class="btn btn-danger" onclick="deleteRecord(' + row.id + ')"><span id="mod" class="material-icons"> delete </span></button></td>';
        rowHtml += '</tr>';
        tableBody.innerHTML += rowHtml;
    }

    updatePagination();
}

function viewDetail(id){
    if (confirm("¿Desea ingresar a ver el detalle de esta entidad?")) {

      window.location.href = '../Entity_controller/list_entity_with_documents_type?id='+id;

 //window.location.href = 'document_types/form_add?id=' + id;
      }
}
function updatePagination() {
    var pagination = document.getElementById("pagination");
    pagination.innerHTML = '';

    var totalPages = Math.ceil(entityData.length / itemsPerPage);
    for (var i = 1; i <= totalPages; i++) {
        var li = document.createElement("li");
        li.className = "page-item";
        var a = document.createElement("a");
        a.className = "page-link";
        a.textContent = i;
        a.href = "javascript:void(0)";
        a.onclick = function () {
            currentPage = parseInt(this.textContent);
            displayData();
        };
        li.appendChild(a);
        pagination.appendChild(li);
    }
}

function editRecord(id) {
    // Realizar una solicitud AJAX al controlador para obtener los detalles de la entidad
    if(confirm("¿Estás seguro de que deseas editar este registro?")){
    fetch('../Entity_controller/get_entity_details?id=' + id, {
        method: 'GET', // Utiliza el método HTTP adecuado para obtener detalles
    })
    .then(response => {
        if (response.ok) {
            return response.json(); // Supongamos que el servidor devuelve datos en formato JSON
        } else {
            alert("Error al obtener detalles de la entidad");
        }
    })
    .then(data => {
        // Manejar la respuesta y abrir una ventana de edición con los datos
        openEditWindow(data); // Supongamos que tienes una función openEditWindow(data) para mostrar una ventana de edición
    })
    .catch(error => {
        console.error('Error:', error);
        alert("Error al obtener detalles de la entidad: " + error);
    });
    }
}



function deleteRecord(id) {
  if (confirm("¿Estás seguro de que deseas eliminar este registro?")) {
     // Hacer una solicitud AJAX al controlador para eliminar el registro con el ID dado
     fetch('../Entity_controller/delete_entity?id=' + id, {
         method: 'DELETE', // O el método HTTP adecuado para eliminar
     })
     .then(response => {
         if (response.ok) {
             // El registro se eliminó con éxito

             alert("Registro eliminado con éxito");
             // Recargar la página o actualizar la lista de registrosf
             location.reload(); // Esto recargará la página
             // O puedes llamar a una función para actualizar la lista de registros
             // updateDataList(); // Implementa esta función para actualizar la lista
         } else {
             // Error al eliminar el registro

             alert("Error al eliminar el registro");
         }
     })
     .catch(error => {
         console.error('Error:', error);
          alert("Error al eliminar el registro: " + error);
     });
 }
}

function openEditWindow(data) {
    // Crear un div para la ventana emergente
    var editWindow = document.createElement("div");
    editWindow.className = "modal fade";
    editWindow.id = "editModal";
    editWindow.tabIndex = -1;
    editWindow.setAttribute("aria-labelledby", "editModalLabel");
    editWindow.setAttribute("aria-hidden", "true");

    // Agregar contenido a la ventana emergente (formulario con Bootstrap)
    editWindow.innerHTML = `
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">Editar Entidad</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="editForm">
                        <div class="form-group">
                        <input type="hidden" id="entity_id" name="entity_id" value="${data.entity.id}">
                            <label for="name">Nombre:</label>
                            <input type="text" class="form-control" id="name" name="name" value="${data.entity.name}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="text" class="form-control" id="email" name="email" value="${data.entity.email}">
                        </div>
                        <div class="form-group">
                            <label for="street">Calle:</label>
                            <input type="text" class="form-control" id="street" name="street" value="${data.direction.street}">
                        </div>
                        <div class="form-group">
                            <label for="number">Numero:</label>
                            <input type="text" class="form-control" id="number" name="number" value="${data.direction.number}">
                        </div>
                        <div class="form-group">
                            <label for="province">Provincia:</label>
                              <select class="form-control" id="province" name="province"></select>
                          </div>
                          <div class="form-group">
                              <label for="city">Ciudad:</label>

                              <select class="form-control" id="city" name="city"></select>
                          </div>
                        <div class="form-group">
                            <label for="phone">Teléfono:</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="${data.entity.phone}">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="save-button">Guardar</button>
                </div>
            </div>
        </div>
    `;

    // Agregar las opciones al menú desplegable


    var tipoDocumentoSelect = editWindow.querySelector("#province");
    data.province.forEach(function (option) {
        var optionElement = document.createElement("option");
        optionElement.value = option.id;
        optionElement.textContent = option.name;
        if (option.id === data.selected_province) {

            optionElement.selected = true;
        }
        tipoDocumentoSelect.appendChild(optionElement);
    });

    var tipoDocumentoSelect = editWindow.querySelector("#city");
    data.city.forEach(function (option) {
        var optionElement = document.createElement("option");
        optionElement.value = option.id;
        optionElement.textContent = option.name;
        if (option.id === data.selected_city) {

            optionElement.selected = true;
        }
        tipoDocumentoSelect.appendChild(optionElement);
    });

    var provinciaSelect = editWindow.querySelector("#province");
    provinciaSelect.addEventListener("change", function () {
    // Obtén el valor seleccionado
    var selectedProvinceId = provinciaSelect.value;

    // Llama a la función que carga las ciudades del controlador
    loadCitiesForProvince(selectedProvinceId);
});


    // Agregar la ventana emergente al cuerpo del documento
    document.body.appendChild(editWindow);

    // Inicializar el modal de Bootstrap
    var modal = new bootstrap.Modal(document.getElementById("editModal"), {});

    // Abrir el modal cuando se hace clic en el botón "Editar"
    modal.show();

    // Definir el comportamiento del botón "Guardar"
    var saveButton = document.getElementById("save-button");
    saveButton.addEventListener("click", function () {
        // Obtener los valores editados del formulario

        var editedData = {
            id:document.getElementById("entity_id").value,
            name: document.getElementById("name").value,
            email: document.getElementById("email").value,
            street: document.getElementById("street").value,
            number: document.getElementById("number").value,
            province_id: document.getElementById("province").value,
            city_id: document.getElementById("city").value,
            phone: document.getElementById("phone").value
        };
        
        // Realizar una solicitud AJAX para guardar los cambios
        fetch('../Entity_controller/save_entity_changes', {
            method: 'POST', // Utiliza el método HTTP adecuado para guardar cambios
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(editedData)
        })
        .then(response => {
            if (response.ok) {

                // Los cambios se guardaron con éxito
                return response.json();
                // Cerrar la ventana emergente

                // location.reload();
                // modal.hide();
                // Actualizar la lista de registros si es necesario
                // updateDataList(); // Implementa esta función para actualizar la lista
            } else {
                // Error al guardar los cambios
                alert("Error al guardar los cambios");
            }
        }).then(data => {
    if (data.success) {
        alert(data.message, true); // Muestra el mensaje como éxito
        location.reload(); // Recargar la página o actualizar la lista de registros
    } else {
        alert(data.message, false); // Muestra el mensaje como error
    }
})
        .catch(error => {
            console.error('Error:', error);
            alert("Error al guardar los cambios: " + error);
        });
    });

    function loadCitiesForProvince(provinceId) {
        // Realiza una solicitud AJAX al controlador para cargar las ciudades correspondientes
        fetch('../City_controller/get_cities?id=' + provinceId, {
            method: 'GET', // Utiliza el método HTTP adecuado para obtener ciudades
        })
        .then(response => {
            if (response.ok) {
                return response.json(); // Supongamos que el servidor devuelve datos de las ciudades en formato JSON
            } else {
                alert("Error al cargar ciudades");
            }
        })
        .then(data => {
            // Maneja la respuesta y actualiza el menú desplegable de ciudades
            updateCityDropdown(data);
        })
        .catch(error => {
            console.error('Error:', error);
            alert("Error al cargar ciudades: " + error);
        });
    }

    function updateCityDropdown(cityData) {
        var ciudadSelect = editWindow.querySelector("#city");
        ciudadSelect.innerHTML = '';

        cityData.forEach(function (option) {
            var optionElement = document.createElement("option");
            optionElement.value = option.id;
            optionElement.textContent = option.name;


            ciudadSelect.appendChild(optionElement);
        });
    }

}


displayData();  // Mostrar los datos al cargar la página
