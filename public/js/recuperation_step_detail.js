function loadRecoverySteps(id) {

    // Realiza una llamada AJAX al controlador de CodeIgniter
    $.ajax({
        url: '../document_steps/get_steps_by_document_entity?id=' + id,
        type: 'GET',
        success: function (data) {
            // Muestra los pasos de recuperación en el modal
            console.log(data);
            showRecoverySteps(data); // No es necesario analizar la respuesta aquí
        },
        error: function (xhr, status, error) {
            console.error('Error en la llamada AJAX:', status, error);
            console.log('Respuesta del servidor:', xhr.responseText);
        }
    });
}
function showRecoverySteps(steps) {
    // Crea el modal siempre antes de mostrarlo
    var modalId = 'recoveryModal';
    var modal = createRecoveryModal(modalId);

    // Limpia el cuerpo del modal
    var modalBody = modal.querySelector('.modal-body');

    // Verifica si el cuerpo del modal existe
    if (!modalBody) {
        console.error('No se encontró el cuerpo del modal');
        return;
    }

    modalBody.innerHTML = '';

    // Crea una tabla para mostrar los pasos de recuperación
    var table = document.createElement('table');
    table.className = 'table';
    var thead = document.createElement('thead');
    var tbody = document.createElement('tbody');

    // Crea la fila de encabezado
    var headerRow = document.createElement('tr');
    var header1 = document.createElement('th');
    header1.textContent = 'Número';
    var header2 = document.createElement('th');
    header2.textContent = 'Descripción';

    headerRow.appendChild(header1);
    headerRow.appendChild(header2);
    thead.appendChild(headerRow);
    table.appendChild(thead);

    // Agrega filas con datos
    steps.forEach(function (step) {
        var row = document.createElement('tr');
        var cell1 = document.createElement('td');
        cell1.textContent = step.number;
        var cell2 = document.createElement('td');
        cell2.textContent = step.description;

        row.appendChild(cell1);
        row.appendChild(cell2);
        tbody.appendChild(row);
    });

    table.appendChild(tbody);
    modalBody.appendChild(table);

    // Muestra el modal
    $(modal).modal('show');
}

function createRecoveryModal(modalId) {
    var modal = document.getElementById(modalId);

    // Crea el modal solo si no existe
    if (!modal) {
        modal = document.createElement('div');
        modal.id = modalId;
        modal.className = 'modal fade';
        modal.setAttribute('tabindex', '-1');
        modal.setAttribute('role', 'dialog');
        modal.setAttribute('aria-labelledby', 'exampleModalLabel');
        modal.setAttribute('aria-hidden', 'true');

        var modalDialog = document.createElement('div');
        modalDialog.className = 'modal-dialog';
        modalDialog.setAttribute('role', 'document');

        var modalContent = document.createElement('div');
        modalContent.className = 'modal-content';

        var modalHeader = document.createElement('div');
        modalHeader.className = 'modal-header';

        var modalTitle = document.createElement('h5');
        modalTitle.className = 'modal-title';
        modalTitle.id = 'exampleModalLabel';
        modalTitle.textContent = 'Pasos de Recuperación';

        var closeButton = document.createElement('button');
        closeButton.type = 'button';
        closeButton.className = 'close';
        closeButton.setAttribute('data-dismiss', 'modal');
        closeButton.setAttribute('aria-label', 'Close');

        var closeIcon = document.createElement('span');
        closeIcon.setAttribute('aria-hidden', 'true');
        closeIcon.textContent = '×';

        closeButton.appendChild(closeIcon);
        modalHeader.appendChild(modalTitle);
        modalHeader.appendChild(closeButton);

        var modalBody = document.createElement('div');
        modalBody.className = 'modal-body';

        var modalFooter = document.createElement('div');
        modalFooter.className = 'modal-footer';

        var closeButtonFooter = document.createElement('button');
        closeButtonFooter.type = 'button';
        closeButtonFooter.className = 'btn btn-secondary';
        closeButtonFooter.setAttribute('data-dismiss', 'modal');
        closeButtonFooter.textContent = 'Cerrar';

        modalFooter.appendChild(closeButtonFooter);

        modalContent.appendChild(modalHeader);
        modalContent.appendChild(modalBody);
        modalContent.appendChild(modalFooter);
        modalDialog.appendChild(modalContent);
        modal.appendChild(modalDialog);

        document.body.appendChild(modal);
    }

    return modal;
}
