function enviarDatosSeleccionados() {
    var checkboxes = document.getElementsByName('seleccionar');
    var datosSeleccionados = [];

    checkboxes.forEach(function (checkbox) {
        if (checkbox.checked) {
            var fila = checkbox.closest('tr');
            var document_id = fila.cells[1].innerText.trim();
            var document_name = fila.cells[2].innerText;
            var expiration_date = fila.cells[3].innerText;
            var number = fila.cells[4].innerText;
            var entity_name = fila.cells[5].innerText;
            var entity_id = fila.cells[6].innerText.trim();

            datosSeleccionados.push({
                document_id: document_id,
                document_name: document_name,
                expiration_date: expiration_date,
                number: number,
                entity_name: entity_name,
                entity_id: entity_id
            });
        }
    });

    console.log(datosSeleccionados);

    fetch('../complain/add_complain', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ datosSeleccionados: datosSeleccionados }),
    })
    .then(response => response.json())  // Asegurarse de que la respuesta se interprete como JSON
    .then(data => {
        if (data.success) {
            alert("Se generó la denuncia");
            window.location.href = '../';
        } else {
            console.log( data.message);
            alert("Error al guardar los cambios: " + data.message);
        }
    })
    .catch(error => {
        console.error('Error:', error);
        alert("Error al realizar la solicitud");
    });
}
