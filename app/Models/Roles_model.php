<?php

namespace App\Models;
use CodeIgniter\Model;


class Roles_model extends Model {

    protected $table = 'roles';

    protected $primaryKey = 'id';
    protected $allowedFields = ['name'];

    protected $useAutoIncrement = true;
    protected $useSoftDeletes = false;

    protected $returnType = 'array';

    const ID_ROL_USER= 2;

}
