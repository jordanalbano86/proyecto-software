<?php

namespace App\Models;
use CodeIgniter\Model;

class Direction_model extends Model {

    protected $table = 'direction';
    protected $primaryKey = 'id';
    protected $allowedFields = ['city_id','street','number'];

    protected $useAutoIncrement = true;
    protected $useSoftDeletes = false;

    protected $returnType = 'array';


}
