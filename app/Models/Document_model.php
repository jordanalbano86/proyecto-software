<?php

namespace App\Models;

use CodeIgniter\Model;


class Document_model extends Model
{
    protected $table = 'documents';
    protected $primaryKey = 'id';
    protected $allowedFields = ['number', 'date_of_issue', 'expiration_date', 'user_id', 'document_entity_id','activated'];
    protected $useAutoIncrement = true;
    protected $useSoftDeletes = false;

    protected $returnType = 'array';


    public function find_documents_by_user($user_id)
    {
        $db = db_connect(); //realiza la conexion con la base de datos
        $query_document = $db->table($this->table . ' d')
            ->select('d.*, e.id entity_id, e.name entity_name, dt.id document_type_id, dt.name document_type_name')
            ->join('document_entity de', 'd.document_entity_id=de.id')
            ->join(' document_types dt', 'de.document_type_id=dt.id')
            ->join('entity e', 'de.entity_id=e.id ')
            ->where('d.user_id', $user_id)
            ->where('d.activated',1)
            ->get();
        // var_dump($query_document->getResultArray());
        // die;
        return $query_document->getResultArray(); //obtiene el arreglo de resultados

    }



    public function find_document($document_id) //esta busqueda arrojará un solo resultado.
    //Trae el documento, su tipo de documento y la entidad a la que pertenece
    {
        $db = db_connect(); //realiza la conexion con la base de datos
        $query_document = $db->table($this->table . ' d')
            ->select('d.*, e.id entity_id, e.name entity_name, dt.id document_type_id, dt.name document_type_name')
            ->join('document_entity de', 'd.document_entity_id=de.id')
            ->join(' document_types dt', 'de.document_type_id=dt.id')
            ->join('entity e', 'de.entity_id=e.id ')
            ->where('d.id', $document_id)
            ->get();

        return $query_document->getResultArray();

    }

}
