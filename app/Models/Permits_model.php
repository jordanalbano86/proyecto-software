<?php

namespace App\Models;
use CodeIgniter\Model;


class Permits_model extends Model {

    protected $table = 'permits';

    protected $primaryKey = 'id';
    protected $allowedFields = ['name','lavel'];

    protected $useAutoIncrement = true;
    protected $useSoftDeletes = false;

    protected $returnType = 'array';


  
}
