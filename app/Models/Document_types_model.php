<?php

namespace App\Models;
use CodeIgniter\Model;


class Document_types_model extends Model {

    protected $table = 'document_types';

    protected $primaryKey = 'id';
    protected $allowedFields = ['name','entity_id'];

    protected $useAutoIncrement = true;
    protected $useSoftDeletes = false;

    protected $returnType = 'array';


  
}
