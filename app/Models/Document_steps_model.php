<?php

namespace App\Models;
use CodeIgniter\Model;

class Document_steps_model extends Model {

    protected $table = 'recuperations_steps';
    protected $primaryKey = 'id';
    protected $allowedFields = ['document_entity_id','number','description'];

    protected $useAutoIncrement = true;
    protected $useSoftDeletes = false;

    protected $returnType = 'array';






}
