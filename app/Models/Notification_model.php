<?php

namespace App\Models;

use CodeIgniter\Model;

class Notification_model extends Model{
    protected $table      = 'notifications';
    protected $primaryKey = 'id';

    protected $allowedFields = ['transmitter', 'receiver', 'message', 'created_on', 'state', 'id','read'];
    protected $returnType     = 'array';
    protected $useSoftDeletes = false;


    protected $useTimestamps = false;

}
