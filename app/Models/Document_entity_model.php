<?php

namespace App\Models;
use CodeIgniter\Model;

class Document_entity_model extends Model {

    protected $table = 'document_entity';
    protected $primaryKey = 'id';
    protected $allowedFields = ['entity_id','document_type_id'];

    protected $useAutoIncrement = true;
    protected $useSoftDeletes = false;

    protected $returnType = 'array';


    public function getAllbyEntity($id) {
    $query = $this->db->table('document_entity as de')
        ->select('de.id, d.id as document_id, d.name as name,de.entity_id')
        ->join('document_types as d', 'd.id = de.document_type_id')
        ->where('de.entity_id', $id)
        ->get();

    return $query->getResultArray();
    }

    //agrego esta consulta para obtener todas las entidad que emiten el tipo de doc pasado por parametro
    public function get_entities_by_document_type($document_type_id) {
        $query = $this->db->table('document_entity de')
            ->select('e.*')
            ->join('entity e', 'e.id = de.entity_id')
            ->where('de.document_type_id', $document_type_id)
            ->get();
    
        return $query->getResultArray();
        }

        //dado un id de entidad y un id de tipo de documento devuelve la unica tupla qu contiene ambas referencias
        public function get_entities_by_document_id($entity_id, $document_type_id) {
            $query = $this->db->table('document_entity de')
            ->where('de.entity_id', $entity_id)
            ->where('de.document_type_id', $document_type_id)
            ->get();
    
            return $query->getResultArray();  
        

            
        }

}
