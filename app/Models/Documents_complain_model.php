<?php

namespace App\Models;
use CodeIgniter\Model;

class Documents_Complain_model extends Model {

    protected $table = 'documents_complains';
    protected $primaryKey = 'id';
    protected $allowedFields = ['complain_id','document_id'];

    protected $useAutoIncrement = true;
    protected $useSoftDeletes = false;

    protected $returnType = 'array';

    public function find_all_by_complain_id($id) {
        $query = $this->db->table('documents_complains as dc')
            ->select('d.expiration_date as expiration_date,d.number as number,d.activated as activated,dt.name as document_name,de.id as document_entity_id ')
            ->join('documents as d', 'dc.document_id = d.id')
            ->join('document_entity as de', 'de.id = d.document_entity_id')
            ->join('document_types as dt', 'dt.id = de.document_type_id ')
            ->where('dc.complain_id', $id)
            ->get();
    
        return $query->getResultArray();
        }




}
