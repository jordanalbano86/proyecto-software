<?php

namespace App\Models;
use CodeIgniter\Model;

class Province_model extends Model {

    protected $table = 'province';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name'];

    protected $useAutoIncrement = true;
    protected $useSoftDeletes = false;

    protected $returnType = 'array';


}
