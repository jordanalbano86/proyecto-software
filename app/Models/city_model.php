<?php

namespace App\Models;
use CodeIgniter\Model;

class City_model extends Model {

    protected $table = 'city';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name','province_id'];

    protected $useAutoIncrement = true;
    protected $useSoftDeletes = false;

    protected $returnType = 'array';


}
