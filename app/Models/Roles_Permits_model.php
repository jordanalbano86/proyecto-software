<?php

namespace App\Models;
use CodeIgniter\Model;


class Roles_Permits_model extends Model {

    protected $table = 'roles_permits';

    protected $primaryKey = 'id';
    protected $allowedFields = ['roles_id','permits_id'];

    protected $useAutoIncrement = true;
    protected $useSoftDeletes = false;

    protected $returnType = 'array';


    public function validate_permits($id_rol, $permiso){
        $tieneAcceso = false;
        $this->select('*');
        $this->join('permits', 'roles_permits.permits_id = permits.id');
        $exists = $this->where(['roles_id' => $id_rol, 'permits.name' => $permiso])->first();
               
        if ($exists){
            $tieneAcceso = true;
        }
        return $tieneAcceso;
    }
  
}
