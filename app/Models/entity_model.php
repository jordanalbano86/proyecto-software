<?php

namespace App\Models;
use CodeIgniter\Model;

class Entity_model extends Model {

    protected $table = 'entity';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name','email','direction_id','phone'];

    protected $useAutoIncrement = true;
    protected $useSoftDeletes = false;

    protected $returnType = 'array';

    public function getAll() {
    $query = $this->db->table('entity as e')
        ->select('e.id, e.name as name, e.email as email, d.street as street, d.number as number, c.name as city, p.name as province, e.phone as phone')
        ->join('direction as d', 'd.id = e.direction_id')
        ->join('city as c', 'd.city_id = c.id')
        ->join('province as p', 'c.province_id = p.id')
        ->get();

    return $query->getResultArray();
}

public function getOne($id) {
    $query = $this->db->table('entity as e')
        ->select('e.id as id, e.name, e.email, d.street, d.number, c.name as city, p.name as province, e.phone as phone')
        ->join('direction as d', 'd.id = e.direction_id')
        ->join('city as c', 'd.city_id = c.id')
        ->join('province as p', 'c.province_id = p.id')
        ->where('e.id', $id) // Agregando la condición where por el ID
        ->get();

    return $query->getResultArray();
}



}
