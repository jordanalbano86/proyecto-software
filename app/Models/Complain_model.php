<?php

namespace App\Models;
use CodeIgniter\Model;

class Complain_model extends Model {

    protected $table = 'complaints';
    protected $primaryKey = 'id';
    protected $allowedFields = ['start_date','end_date','modification_date','status_id'];

    protected $useAutoIncrement = true;
    protected $useSoftDeletes = false;

    protected $returnType = 'array';


    public function find_all_by_user_id($id) {
        $query = $this->db->table('documents_complains as dc')
            ->select('c.id,c.start_date,c.end_date,modification_date,s.name as status_name')
            ->join('documents as d', 'd.id = dc.document_id')
            ->join('complaints as c', 'c.id = dc.complain_id')
            ->join('status as s', 's.id = c.status_id')
            ->where('d.user_id', $id)
            ->groupby('c.id')
            ->get();
        // var_dump($query->getResultArray()); 
        // die;
        return $query->getResultArray();
        }

        public function find_all() {
            $query = $this->db->table('documents_complains as dc')
                ->select('c.id,c.start_date,c.end_date,modification_date,s.name as status_name,s.id as status_id')
                ->join('documents as d', 'd.id = dc.document_id')
                ->join('complaints as c', 'c.id = dc.complain_id')
                ->join('status as s', 's.id = c.status_id')
                ->groupby('c.id')
                ->get();
            return $query->getResultArray();
            }
        
        public function get($id) {
            $query = $this->db->table('complaints as c')
                ->select('c.id,c.start_date,c.end_date,modification_date,s.name as status_name')
                ->join('status as s', 's.id = c.status_id')
                ->where('c.id', $id)
                
                ->get();
        
            return $query->getResultArray();
            }
    

        

}
