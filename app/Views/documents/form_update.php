<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

<div>

    <?php //var_dump($entities); ?>
    <?php //var_dump($documents); ?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Nueva documentación</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Mi billetera / Mi documentación</a></li>
                        <li class="breadcrumb-item active">Nueva documentación</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="card">
        <!-- /.content-header -->
        <div class="container-fluid">

            <?php $errors = validation_errors();
            if (!empty($errors)): ?>
                <div class="alert alert-danger">
                    <!-- Aquí puedes mostrar los mensajes de error -->
                    <?php foreach ($errors as $error): ?>
                        <p>
                            <?php echo esc($error) ?>
                        </p>
                    <?php endforeach ?>
                </div>
            <?php endif ?>

            <form id="form_add" method="post" action="<?php echo base_url(); ?>documents/update">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tipo de Documento</label>
                                <select disabled name="document_type_id" disabled="disabled"
                                    class="form-control select2bs4 bg-light text-dark" style="width: 100%;">
                                    <?php foreach ($documents as $doc): ?>
                                        <option selected="selected" id="document_type_id"
                                            value="<?php $doc['document_type_id'] ?>">
                                            <?php echo $doc['document_type_name']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <input type="hidden" name="document_type_id"
                                    value="<?php echo $doc['document_type_id']; ?>">
                            </div>

                            <div class="form-group">
                                <label>Entidad Emisora</label>
                                <select name="entity_id" class="form-control select2bs4" style="width: 100%;">
                                    <?php foreach ($documents as $doc) { ?>
                                        <option selected="selected" id="entity_name"
                                            value="<?php echo $doc["entity_id"] ?>">
                                            <?php echo $doc['entity_name']; ?>
                                        </option>

                                        <?php foreach ($entities as $ent) { ?>
                                            <?php
                                            if ($ent['id'] !== $doc['entity_id']) { ?>
                                                <option id="entity_id" value="<?php echo $ent["id"] ?>">
                                                    <?php echo $ent['name']; ?>
                                                </option>
                                            <?php } ?>
                                        <?php } ?>

                                    <?php } ?>




                                </select>
                                <input type="hidden" name="id" value="<?php echo $doc["id"] ?>" />
                            </div>

                            <!-- /.form-group -->
                            <div class="form-group">
                                <label>Número</label>
                                <div class="input-group">
                                    <input name="number" value="<?= old('username') ?? $doc['number'] ?>" type="text"
                                        class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Fecha de Emisión</label>
                                <div class="input-group">
                                    <input name="date_of_issue"
                                        value="<?= old('date_of_issue') ?? $doc["date_of_issue"] ?>" type="date"
                                        class=" text-white form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Fecha de Vencimiento</label>
                                <div class="input-group">
                                    <input name="expiration_date"
                                        value="<?= old('expiration_date') ?? $doc["expiration_date"] ?>" type="date"
                                        class="form-control">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <!-- /.form-group -->
                        </div>

                    </div>
                    <!-- /.row -->
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>


<script>
    document.addEventListener('DOMContentLoaded', function () {
        // Manejar el cambio en el combo y hacer la llamada AJAX
        $('#combo_document_types').on('change', function () {
            var document_type_id = $(this).val();

            // Realizar la llamada AJAX con la variable
            $.ajax({
                url: '<?php echo base_url() ?>documents/form_add/entities/' + document_type_id,
                method: 'GET',
                dataType: 'json',
                success: function (data) {
                    $('#combo_entities').removeClass('bg-light text-dark');
                    $('#combo_entities').prop('disabled', false);
                    $('#combo_entities').empty();
                    $('#combo_entities').append($('<option>', {
                        value: '',
                        text: 'Elige una opción',
                        disabled: true,
                        selected: true
                    }));
                    $.each(data.entities, function (index, entity) {
                        $('#combo_entities').append($('<option>', {
                            value: entity.id,
                            text: entity.name
                        }));
                    });
                },
                error: function (error) {
                    console.error('Error en la solicitud AJAX', error);
                }
            });
        });
    });
</script>

<?= $this->endSection() ?>