<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

<div>
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Nueva documentación</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Mi billetera / Mi documentación</a></li>
            <li class="breadcrumb-item active">Nueva documentación</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <div class="card">
    <!-- /.content-header -->
    <div class="container-fluid">
     
      <?php $errors = validation_errors();
      if (!empty($errors)): ?>
        <div class="alert alert-danger">
          <!-- Aquí puedes mostrar los mensajes de error -->
          <?php foreach ($errors as $error): ?>
            <p>
              <?php echo esc($error) ?>
            </p>
          <?php endforeach ?>
        </div>
      <?php endif ?>


      <form id="form_add" method="post" action="<?php echo base_url(); ?>documents/new">
        <!-- /.card-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Tipo de Documento</label>
                <select id="combo_document_types" name="document_type_id" class="form-control select2bs4"
                  style="width: 100%;">
                  <option value="" disabled selected>Elige una opción</option>
                  <?php foreach ($data as $doc): ?>
                    <option id="document_type_id" value="<?php echo $doc["id"] ?>">
                      <?php echo $doc['name']; ?>
                    </option>
                  <?php endforeach; ?>
                </select>
              </div>
              <!-- /.form-group -->

              <div class="form-group">
                <label>Entidad Emisora</label>
                <select id="combo_entities" name="entity_id" class="form-control select2bs4 bg-light text-dark"
                  style="width: 100%;" disabled>
                </select>
              </div>

              <div class="form-group">
                <label>Número</label>
                <div class="input-group">
                  <input name="number" type="text" class="form-control" value="<?= old('number')?>" required>
                </div>
              </div>

              <div class="form-group">
                <label>Fecha de Emisión</label>
                <div class="input-group">
                  <input name="date_of_issue" type="date" class=" text-white form-control" value="<?= old('date_of_issue')?>" required>
                </div>
              </div>

              <div class="form-group">
                <label>Fecha de Vencimiento</label>
                <div class="input-group">
                  <input name="expiration_date" type="date" class="form-control" value="<?= old('expiration_date')?>">
                </div>
              </div>

              <button type="submit" class="btn btn-primary">Agregar</button>
              <!-- /.form-group -->
            </div>

          </div>
          <!-- /.row -->
        </div>
      </form>


    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
  document.addEventListener('DOMContentLoaded', function () {
    // Manejar el cambio en el combo y hacer la llamada AJAX
    $('#combo_document_types').on('change', function () {
      var document_type_id = $(this).val();

      // Realizar la llamada AJAX con la variable
      $.ajax({
        url: '<?php echo base_url() ?>documents/form_add/entities/' + document_type_id,
        method: 'GET',
        dataType: 'json',
        success: function (data) {

          if (data.entities && data.entities.length > 0) {
            $('#combo_entities').removeClass('bg-light text-dark');
            $('#combo_entities').prop('disabled', false);
            $('#combo_entities').empty();
            $('#combo_entities').append($('<option>', {
              value: '',
              text: 'Elige una opción',
              disabled: true,
              selected: true
            }));
            $.each(data.entities, function (index, entity) {
              $('#combo_entities').append($('<option>', {
                value: entity.id,
                text: entity.name
              }));
            });
          } else {
            $('#combo_entities').append($('<option>', {
              value: "",
              text: "No existe entidad que emita el documento"
            }));
          }





        },
        error: function (error) {
          console.error('Error en la solicitud AJAX', error);
        }
      });
    });
  });
</script>

<?= $this->endSection() ?>
