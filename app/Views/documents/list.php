<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

<script>
    function open_form() {
        location.href = "<?php echo base_url(); ?>documents/form_add";
    }
</script>

<div>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Mi documentación</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Mi billetera</a></li>
                        <li class="breadcrumb-item active">Mi documentación</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="card">
        <!-- /.content-header -->
        <div class="container-fluid">
            <div class="card-body">
                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="col">Tipo de documento</th>
                            <th scope="col">Entidad emisora</th>
                            <th scope="col">Número</th>
                            <th scope="col">Fecha de Emisión</th>
                            <th scope="col">Fecha de Vencimiento</th>
                            <th scope="col">Acci&oacute;n</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $doc): ?>
                            <tr>
                                <td>
                                    <?php echo $doc['document_type_name']; ?>
                                </td>
                                <td>
                                    <?php echo $doc['entity_name']; ?>
                                </td>
                                <td>
                                    <?php echo $doc['number']; ?>
                                </td>
                                <td>
                                    <?php echo $doc['date_of_issue']; ?>
                                </td>

                                <td>
                                    <?php echo $doc['expiration_date']; ?>

                                </td>
                                <td>

                                    <a href="#" onclick="confirmDelete(<?php echo $doc['id']; ?>)"><span
                                            id="del" class="material-icons" style="cursor: pointer; color: #c71c1c;"> delete
                                        </span></a>
                                    <a
                                        href="<?php echo base_url(); ?>documents/form_update/<?php echo $doc['id'];?>">
                                        <span id="mod" class="material-icons"> update </span> </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>

                </table>
            </div>
            <div class="card-footer">
                <button id="add" type="button" class="btn btn-primary" name="button" onclick="open_form()">Agregar
                    documentación</button>
            </div>
        </div>
    </div>
</div>


<script>
    function confirmDelete(documentId) {
        // Muestra un cuadro de confirmación
        var result = confirm("¿Estás seguro de que deseas eliminar este documento?");

        // Si el usuario hace clic en "Aceptar", redirige a la URL de eliminación
        if (result) {
            var deleteUrl = "<?php echo base_url('documents/delete/'); ?>" + documentId;
            window.location.href = deleteUrl;
        }
    }
</script>
<?= $this->endSection() ?>