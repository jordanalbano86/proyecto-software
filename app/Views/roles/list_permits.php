<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>
  <div>
      <!-- Content Header (Page header) -->
      <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Permisos del Rol</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Roles</a></li>
              <li class="breadcrumb-item active">Permisos</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    
    <div class="container-fluid">
        <div class="card-header">
                <h3 class="card-title">Seleccione los Permisos</h3>
              </div>
              <div class="card-body">
            <div class="form-group">
            <form id="quickForm" method="post" action="<?php echo base_url();?>roles/add_permits">
              <div class="row" style="margin: 10px;">
                <input type="hidden" name="id_rol" value="<?= $id_rol;?>">
                <?php foreach ($permisos as $permiso){?>
                  <div class="checkbox col-sm-4">
                <input name="permisos[]" class="form-check-input" type="checkbox" value="<?= $permiso['id'];?>"
                <?php if (isset($asignado[$permiso['id']]))
                {
                  echo 'checked';
                }?>>
                <label class="form-check-label"> <?= $permiso['name'];?></label><br>
                </div>
                <?php }?>
                
              </div>
            </div>
          


        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
    </form>
    </div>
    </div>
 <?= $this->endSection() ?>
