<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>


    <div>
         <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Roles del Sistema</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Roles</a></li>
              <li class="breadcrumb-item active">Listado</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <div class="card">
        <!-- /.content-header -->
       <div class="container-fluid">
            <div class="card-body">
                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                            <th scope="col">ID</th><th scope="col">Nombre</th><th scope="col">Acci&oacute;n</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($roles as $rol): ?>
                                <tr>
                                    <td>
                                        <?= $rol['id']; ?>
                                    </td>
                                    <td>
                                        <?= $rol['name']; ?>
                                    </td>
                                    <td>
                                      <div class="d-flex align-items-center">
                                       <form method="post" action="<?= base_url();?>roles/list_for_rol">
                                            <input type="hidden" name="id" value="<?= $rol['id'];?>">
                                            <button type="submit" class="btn btn-success mr-2"> Permisos</button>
                                        </form>
                                        <form method="get" action="<?= base_url();?>roles/form_update/<?= $rol['id'];?>">
                                            <button type="submit" class="btn btn-info mr-2">  <i class="far fa-edit"></i></button>
                                        </form>
                                        <form method="post" action="<?= base_url();?>roles/delete">
                                            <input type="hidden" name="id" value="<?= $rol['id'];?>">
                                            <button type="submit" class="btn btn-danger ">  <i class="far fa-trash-alt"></i></button>
                                        </form>
                                      </div>


                                    </td>
                                </tr>
                                  <?php endforeach; ?>
                        </tbody>

                </table>
                <hr>
                <nav>
                    <ul class="l-button" style="list-style:none; padding-left: 5px;">
                      <li >
                         <a class="btn btn-primary" href="<?= base_url();?>roles/form_add">Agregar Rol</a>
                      </li>
                      <li >
                         <a class="btn btn-success" href="<?= base_url();?>roles/form_permits_add">Nuevo Permiso</a>
                      </li>
                    </ul>
                </nav>
            </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>
