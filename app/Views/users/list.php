<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

    <div>
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Usuarios</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Usuarios</a></li>
                            <li class="breadcrumb-item active">Listado</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <div class="card">
            <!-- /.content-header -->
            <div class="container-fluid">
                <div class="card-body">
                    <table class="table table-striped" id="dataTable"  >
                        <thead>
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellido</th>
                            <th scope="col">Nombre de usuario</th>
                            <th scope="col">Email</th>
                            <th scope="col">Acci&oacute;nes</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php /** @var TYPE_NAME $users */
                        foreach ($users as $user): ?>
                            <tr>
                                <td id="list_name" ><?= $user['name']; ?></td>
                                <td id="list_lastname"><?= $user['lastname']; ?></td>
                                <td id="list_username"><?= $user['username']; ?></td>
                                <td id="list_email"> <?= $user['email']; ?></td>
                                <td>
                                    <div style="display: -webkit-box">
                                        <button id="view-detailss" type="button" class="btn btn-info" data-userid="<?= $user['id'];?>" >
                                            <span class="material-icons" >visibility</span>
                                        </button>
                                        <form method="post" action="<?= base_url();?>users/form_update">
                                            <input type="hidden" name="id" value="<?= $user['id'];?>">
                                            <button type="submit" class="btn btn-secondary"><span id="mod" class="material-icons"> edit </span></button>
                                        </form>
                                        <form method="post" action="<?= base_url();?>users/delete">
                                            <input type="hidden" name="id" value="<?= $user['id'];?>">
                                            <button type="submit" class="btn btn-danger" title="Actualizar Usuario">
                                                <span id="mod" class="material-icons"> delete </span>
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="modal-infoo">
    <div class="modal-dialog">
        <div class="modal-content bg-info">
            <div class="modal-header">
                <h4 class="modal-title">Detalle del usuario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>

    $(document).ready(function() {
        console.log('asdsadas');
        // Cuando se hace clic en el botón "Ver detalles"
        $('#dataTable').on('click', '#view-detailss', function() {
            $('#modal-infoo').modal('show');
            const name = $(this).closest('tr').find('#list_name').text();
            const lastname = $(this).closest('tr').find('#list_lastname').text();
            const username = $(this).closest('tr').find('#list_username').text();
            const email = $(this).closest('tr').find('#list_email').text();
            // Actualizar el modal con los detalles del usuario
            $('#modal-infoo').find('.modal-body').html('' +
                '<p>Nombre: <label>' + name + '</label></p>' +
                '<p>Apellido: <label>' + lastname + '</label></p>' +
                '<p>Correo electrónico: <label>' + email + '</label></p>' +
                '<p>Username: <label>' + username + '</label></p>'
            );

         })
        });
</script>

<?= $this->endSection() ?>
