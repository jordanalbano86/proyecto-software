<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

    <!-- Content Header (Page header) -->
    <div class="content-header" >
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Registrar usuario</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Usuarios</a></li>
                        <li class="breadcrumb-item active">registrar</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

<form id="quickForm" method="post" action="<?php echo base_url();?>users/register">
    <div class="card-body">
        <div class="form-group">
            <label>Nombre</label>
            <input type="text" name="name" class="form-control" placeholder="Ingrese el nombre" required>
        </div>
        <div class="form-group">
            <label>Apellido</label>
            <input type="text" name="lastname" class="form-control" placeholder="Ingrese el nombre" required>
        </div>
        <div class="form-group">
            <label>Correo electronico</label>
            <input type="email" name="email" class="form-control" placeholder="Ingrese el correo electronico" required>
        </div>
        <div class="form-group">
            <label>Nombre de usuario</label>
            <input type="text" name="username" class="form-control" placeholder="Ingrese el nombre de usuario" required>
        </div>
        <div class="form-group">
            <label>Contraseña</label>
            <input type="text" name="password" class="form-control" placeholder="Ingrese la contraseña" required>
        </div>
        <div class="form-group">
            <label>Repita la contraseña</label>
            <input type="text" name="password-repeat" class="form-control" placeholder="Repita la contraseña" required>
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <button id="ejemplo" type="submit" class="btn btn-primary">Confirmar</button>
    </div>
</form>
    <!-- Main content -->

<?= $this->endSection() ?>
