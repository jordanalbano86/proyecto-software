<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Modificar Usuario</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Usuarios</a></li>
                    <li class="breadcrumb-item active">Actualizar</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<form method="post" action="<?php echo base_url();?>users/update">

    <div class="card-body">
        <div class="form-group">
            <label>Nombre</label>
            <input type="text" name="name" class="form-control" placeholder="Ingrese el nombre" value="<?php echo $name; ?>" required>
        </div>

        <div class="form-group">
            <label>Apellido</label>
            <input type="text" name="lastname" class="form-control" placeholder="Ingrese el apellido" value="<?php echo $lastname ?>" required>
        </div>

        <div class="form-group">
            <label>Nombre de usuario</label>
            <input type="text" name="username" class="form-control" placeholder="Ingrese el nombre de usuario" value="<?php echo $username; ?>" required>
        </div>

        <div class="form-group">
            <label>Correo electronico</label>
            <input type="text" name="email" class="form-control" placeholder="Ingrese el correo electronico" value="<?php echo $email; ?>" required>
        </div>

        <div class="form-group">
            <label>Contraseña actual</label>
            <input type="text" name="password" class="form-control" placeholder="Ingrese la contraseña actual" required>
        </div>

        <div class="form-group">
            <label>Nueva contraseña</label>
            <input type="text" name="password-new" class="form-control" placeholder="Ingrese la contraseña" >
        </div>

        <div class="form-group">
            <label>Repita la nueva contraseña</label>
            <input type="text" name="password-new-repeat" class="form-control" placeholder="Repita la nueva contraseña"  >
        </div>

        <input type="hidden" name="id" value="<?php echo $id; ?>">
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Confirmar</button>
    </div>
</form>
<?= $this->endSection() ?>
