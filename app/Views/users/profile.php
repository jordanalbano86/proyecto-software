<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Mi Perfil</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Mi Perfil</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="<?= base_url();?>dist/img/user8-128x128.jpg"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center"><?= $_SESSION['name'].' '.$_SESSION['lastname'];?></h3>

                <p class="text-muted text-center"></p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Rol</b> <a class="float-right"><?= $_SESSION['rol'];?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right"><?= $_SESSION['email'];?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Usuario</b> <a class="float-right"><?= $_SESSION['username'];?></a>
                  </li>
                </ul>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#notify" data-toggle="tab">Notificaciones</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Editar Perfil</a></li>
                  <li class="nav-item"><a class="nav-link" href="#setting-pass" data-toggle="tab">Cambiar Contraseña</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="notify">
                    <!-- Post -->

                  </div>
                  <?php validation_list_errors();?>
                  <div class="tab-pane" id="settings">
                    <form method="post" class="form-horizontal" action="<?php echo base_url();?>users/update_citizen">
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nombre</label>
                        <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" placeholder="Ingrese su nombre" value="<?= set_value('name', $_SESSION['name']);?>">
                        <?php if (validation_show_error('name')){?>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                          <?= validation_show_error('name');?>
                        </div> <?php } ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Apellido</label>
                        <div class="col-sm-10">
                        <input type="text" name="lastname" class="form-control" placeholder="Ingrese su apellido" value="<?= set_value('lastname', $_SESSION['lastname']);?>">
                        <?php if (validation_show_error('lastname')){?>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                          <?= validation_show_error('lastname');?>
                        </div> <?php } ?>
                      </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Usuario</label>
                        <div class="col-sm-10">
                        <input type="text" name="username" class="form-control" placeholder="Ingrese su usuario" value="<?= set_value('username', $_SESSION['username']);?>">
                        <?php if (validation_show_error('username')){?>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                          <?= validation_show_error('username');?>
                        </div> <?php } ?>
                      </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                        <input type="text" name="email" class="form-control" placeholder="Ingrese su email" value="<?= set_value('email', $_SESSION['email']);?>">
                        <?php if (validation_show_error('email')){?>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                          <?= validation_show_error('email');?>
                        </div> <?php } ?>
                      </div>
                      </div>
                      
                      <input type="hidden" name="id" value="<?= $_SESSION['id']; ?>">
                     
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-primary">Confirmar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="tab-pane" id="setting-pass">
                    <form method="post" class="form-horizontal" action="<?php echo base_url();?>users/update_password">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Contraseña actual</label>
                        <div class="col-sm-9">
                        <input type="password" name="password" class="form-control" placeholder="Ingrese contraseña actual" required>  
                      </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nueva Contraseña</label>
                        <div class="col-sm-9">
                        <input type="password" name="password-new" class="form-control" placeholder="Ingrese contraseña nueva" required>  
                      </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Repita Contraseña</label>
                        <div class="col-sm-9">
                        <input type="password" name="password-new-repeat" class="form-control" placeholder="Repita la contraseña nueva" required>  
                      </div>
                      </div>
                      <input type="hidden" name="id" value="<?= $_SESSION['id']; ?>">
                     
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-primary">Confirmar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
        <script src="<?= base_url();?>plugins/jquery/jquery.min.js"></script>
        <script>
            $(document).ready(function(){
                var id = <?= $_SESSION['id']; ?>;
                $.ajax({
                    url: "<?php echo base_url()?>notifications/users",

                    type: 'get',
                    success: function(notification){
                        var notifications = JSON.parse(notification);
                        console.log("es aca;")
                        console.log(notifications);
                   
                        
                        $.each(notifications, function(i, item) {
                            let read = undefined;
                            console.log(item);
                            var html = '<div class="post"> <div class="user-block"><img class="img-circle img-bordered-sm" src="<?= base_url();?>dist/img/user8-128x128.jpg" alt="user image"> <span class="username" id=id-notification><a href="#">'+' De ' +  item.transmitter_details.email + ' </a> <label  style=float:inline-end><input type="checkbox" onclick=changeCheck('+item.id +') id="'+item.id+'" name='+item.id+' value="first_checkbox" /> Leido '+ '</label ><br /> <span class="description">Enviado el ' +item.created_on+'</span><p> </div> '+item.message+'</p></div>';
                            $('#notify').append(html);
                            var checkbox = document.getElementById(item.id);
                            checkbox.checked = item.read === '1';
                            if(item.read === '1'){
                                document.getElementById(item.id).disabled = true;
                            }
                        });
                    }
                });
            });
            function changeCheck(id){

                console.log(id);
                $.ajax({
                    url: "<?php echo base_url()?>notifications/read",
                    type: 'post',
                    data: {id: id},
                    success: function(notification){
                        console.log(JSON.parse(notification));
                        document.getElementById(id).disabled = true;
                    }
                });
            }
        </script>

<?= $this->endSection() ?>
