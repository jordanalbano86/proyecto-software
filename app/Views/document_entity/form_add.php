<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

      <!-- Content Header (Page header) -->
      <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Nuevo Tipo de Documento</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Tipos de Documentos</a></li>
              <li class="breadcrumb-item active">Nuevo Tipo</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <form id="quickForm" method="post" action="<?php echo base_url();?>document_entity/add">

        <div class="card-body">
            <div class="form-group">
                <label>Seleccione un Tipo de Documento que desea dar de alta en su institucion</label>
                  <select name="document_type_id" id="opciones">
                      <?php
                      foreach ($document_types as $document_type) {
                          echo '<option value="' . $document_type['id'] . '">' . $document_type['name'] . '</option>';
                      }
                      ?>
                    </select>


              <!--  <input type="text" name="name" class="form-control"  required>-->
            </div>
            <input type="hidden" name="id" class="form-control" value="<?= $id ?>">


        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Confirmar</button>
        </div>
    </form>

 <?= $this->endSection() ?>
