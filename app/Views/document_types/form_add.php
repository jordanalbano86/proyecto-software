<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

      <!-- Content Header (Page header) -->
      <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Nuevo Tipo de Documento</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Tipos de Documentos</a></li>
              <li class="breadcrumb-item active">Nuevo Tipo</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <?php validation_list_errors();?>
    <form id="quickForm" method="post" action="<?php echo base_url();?>document_types/add">

        <div class="card-body">
            <div class="form-group">
                <label>Nombre del Tipo de documento</label>
                <input type="text" name="name" class="form-control" value="<?= set_value('name');?>">
                <?php if (validation_show_error('name')){?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                   <?= validation_show_error('name');?>
                </div> <?php } ?>
            </div>
          


        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Confirmar</button>
        </div>
    </form>

 <?= $this->endSection() ?>
