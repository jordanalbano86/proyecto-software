<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

<script>
    function open_form() {
        location.href = "<?php echo base_url(); ?>document_types/form_add";
    }
</script>

<div>
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Tipos de Documentos</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Tipos de Documentos</a></li>
            <li class="breadcrumb-item active">Listado</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <div class="card">
    <!-- /.content-header -->
    <div class="container-fluid">
      <div class="card-body">
        <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th scope="col">Nombre</th>
              <th scope="col">Acci&oacute;n</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($data as $tipo): ?>
              <tr>
                <td>
                  <?= $tipo['name']; ?>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <form method="get" action="<?= base_url(); ?>document_types/form_update/<?= $tipo['id']; ?>">
                      <button type="submit" class="btn btn-info mr-2"> <span id="mod" class="material-icons"> edit
                        </span></button>
                    </form>
                    <form method="post" action="<?= base_url(); ?>document_types/delete">
                      <input type="hidden" name="id" value="<?= $tipo['id']; ?>">
                      <button type="submit" class="btn btn-danger "> <span id="mod" class="material-icons"> delete
                        </span></button>
                    </form>
                  </div>


                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>

                </table>
                <hr>
                <nav>
                    <ul class="l-button" style="list-style:none; padding-left: 5px;">
                      <li >
                         <a class="btn btn-primary" href="<?= base_url();?>document_types/form_add">Agregar Nuevo</a>

                      </li>
                    </ul>
                </nav>
            </div>
    </div>
  </div>
</div>
<?= $this->endSection() ?>
