<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>Perdi Mi Billetera - APP</b>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Inicio de Sesion</p>

      <form action="<?php echo base_url();?>users/login" method="post">
        <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Email" id="email" name="email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
            <input type="password" class="form-control" placeholder="Password" id="password" name="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
          <button id="role_id" name="role_id" value="2" hidden></button>
        <div class="row">
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Ingresar</button>
          </div>
          <!-- /.col -->
          <p class="mb-0">
        <a href="<?= base_url();?>form_register" class="text-center">Registrarse</a>
      </p>
        </div>
      </form>

      
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
</body>
</html>
