<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <title>Lista de Denuncias</title>

    <!-- Estilos adicionales para el mensaje hover -->
    <style>
        .btn-info:hover,
        .btn-success:hover {
            cursor: pointer;
        }
    </style>
</head>

<div class="container">
    <h2>Denuncias</h2>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Identificador</th>
                <th>Fecha de Inicio</th>
                <th>Fecha de finalizacion</th>
                <th>Fecha de actualizacion</th>
                <th>Estado</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody id="table-body">
            <?php foreach ($complains as $value): ?>
                <tr>
                    <td><?= $value['id']; ?></td>
                    <td><?= $value['start_date']; ?></td>
                    <td><?= $value['end_date']; ?></td>
                    <td><?= $value['modification_date']; ?></td>
                    <td><?= $value['status_name']; ?></td>
                    <td>
                        <?php if ($value['status_id'] === "1"): ?>
                            <button data-toggle="tooltip" data-placement="top" title="Iniciar" class="btn btn-info" onclick="initComplain(<?= $value['id']; ?>)"><span id="mod" class="material-icons"> add </span></button>            
                        <?php endif; ?>
                        <?php if ($value['status_id'] === "2"): ?>
                            <button data-toggle="tooltip" data-placement="top" title="Finalizar" class="btn btn-success" onclick="termComplain(<?= $value['id']; ?>)"><span id="mod" class="material-icons"> done </span></button>            
                        <?php endif; ?>
                        <a class="btn btn-info" href="<?= base_url('complain/view_detail?id=' . $value['id']) ?>">
                            <span id="mod" class="material-icons">search</span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script src="<?= base_url('js/complain.js') ?>"></script>
<!-- Scripts adicionales para habilitar el tooltip de Bootstrap -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<?= $this->endSection() ?>
