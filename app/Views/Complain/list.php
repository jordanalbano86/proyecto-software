<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <title>Lista de Denuncias</title>
</head>

<div class="container">
    <h2>Denuncias</h2>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Identificador</th>
                <th>Fecha de Inicio</th>
                <th>Fecha de finalizacion</th>
                <th>Fecha de actualizacion</th>
                <th>Estado</th>
                <th>Ver detalle</th>
            </tr>
        </thead>
        <tbody id="table-body">
            <?php foreach ($complains as $value): ?>
                <tr>
      
                    <td><?= $value['id']; ?></td>
                    <td><?= $value['start_date']; ?></td>
                    <td><?= $value['end_date']; ?></td>
                    <td><?= $value['modification_date']; ?></td>
                    <td><?= $value['status_name']; ?></td>
                    <td>
                        <a class="btn btn-info" href="<?= base_url('complain/view_detail?id=' . $value['id']) ?>">
                            <span id="mod" class="material-icons">search</span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
   </div>


<?= $this->endSection() ?>
