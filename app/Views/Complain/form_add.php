<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <title>Tabla de Datos</title>
</head>

<div>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Documentación Extraviada</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Denuncias</a></li>
                        <li class="breadcrumb-item active">Nueva</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="card">
        <!-- /.content-header -->
        <div class="container-fluid">
            <div class="card-body">
                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Seleccionar</th>
                            <th>Nombre del Documento</th>
                            <th>Fecha de vencimiento</th>
                            <th>Numero</th>
                            <th>Entidad emisora</th>
                        </tr>
                    </thead>
                    <tbody id="table-body">
                        <?php foreach ($wallet as $value): ?>
                            <tr>

                                <td><input type="checkbox" name="seleccionar"></td>
                                <td style="display: none;">
                                    <?=$value['id'];?>
                                </td> <!-- Campo oculto -->
                                <td>
                                    <?= $value['document_type_name']; ?>
                                </td>
                                <td>
                                    <?= $value['expiration_date']; ?>
                                </td>
                                <td>
                                    <?= $value['number']; ?>
                                </td>
                                <td>
                                    <?= $value['entity_name']; ?>
                                </td>
                                <td style="display: none;">
                                    <?= $value['entity_id']; ?>
                                </td> <!-- Campo oculto -->

                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <hr>
                <button class="btn btn-primary" onclick="enviarDatosSeleccionados()">Enviar Datos Seleccionados</button>
            </div>
        </div>
    </div>

    <!-- JavaScript para manejar la paginación y cargar datos -->
    <script src="<?= base_url('js/send_complain.js') ?>"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->

    <?= $this->endSection() ?>