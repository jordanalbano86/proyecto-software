<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <title>Lista de Denuncias</title>
</head>

<div class="container">
    <h2>Denuncia</h2>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Identificador</th>
                <th>Fecha de Inicio</th>
                <th>Fecha de finalizacion</th>
                <th>Fecha de actualizacion</th>
                <th>Estado</th>
        
            </tr>
        </thead>
        <tbody id="table-body">
            
                <tr>
      
                    <td><?= $complain[0]['id']; ?></td>
                    <td><?= $complain[0]['start_date']; ?></td>
                    <td><?= $complain[0]['end_date']; ?></td>
                    <td><?= $complain[0]['modification_date']; ?></td>
                    <td><?= $complain[0]['status_name']; ?></td>
                   
                </tr>
        </tbody>
    </table>
    <hr>
    <h2>Documentos involucrados</h2>
    <div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Fecha de Expiracion</th>
                    <th>Numero</th>
                    <th>Ver pasos de recuperacion</th>
            
                </tr>
            </thead>
            <tbody id="table-body">
                <?php foreach ($documents as $document): ?>
                    <tr>
                        <td><?= $document['document_name']; ?></td>
                        <td><?= $document['expiration_date']; ?></td>
                        <td><?= $document['number']; ?></td>
                        <td><button class="btn btn-secondary" onclick="loadRecoverySteps(<?= $document['document_entity_id']; ?>)"><span id="mod" class="material-icons"> search </span></button></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
   </div>

<script src="<?= base_url('js/recuperation_step_detail.js') ?>"></script>
<?= $this->endSection() ?>
