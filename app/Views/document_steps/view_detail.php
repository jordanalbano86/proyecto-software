<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="<?=base_url('css/entity/list_entitys.css')?>" />
    <title>Tabla de Datos</title>
</head>

    <div class="container">
        <h2>Tabla de Datos</h2>
        <table class="table table-bordered">
          <thead>
              <tr>
                  <th>Numero</th>
                  <th>Descripcion</th>
                  <th>Opciones</th>
              </tr>
          </thead>
          <tbody id="table-body">
            <?php foreach ($document_steps as $value): ?>


                  <tr>
                      <td><?= $value['number']; ?></td>
                      <td><?= $value['description']; ?></td>
                      <td>
                        <button class="btn btn-secondary" onclick="editRecord(<?= $value['id']; ?>,<?= $id ?>)"><span id="mod" class="material-icons"> edit </span></button>
                        <button class="btn btn-danger" onclick="deleteRecord(<?= $value['id']; ?>)"><span id="mod" class="material-icons"> delete </span></button>
                      </td>

                  </tr>

            <?php endforeach; ?>

          </tbody>
      </table>
    <hr>
    <form method="get" action="<?= base_url();?>document_steps/form_add">
        <input type="hidden" name="id" class="form-control" value="<?= $id ?>">
        <button type="submit" class="btn btn-primary">  Agregar paso</button>
    </form>
    </div>

         <!-- Content Header (Page header) -->

    <!-- JavaScript para manejar la paginación y cargar datos -->
      <script src="<?= base_url('js/document_steps.js') ?>"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<?= $this->endSection() ?>
