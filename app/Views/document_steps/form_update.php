<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>

      <!-- Content Header (Page header) -->
      <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Modificar Paso</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#"></a></li>
              <li class="breadcrumb-item active"></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <form method="post" action="<?php echo base_url();?>document_steps/update">

        <div class="card-body">
            <div class="form-group">
                <label>Descripcion</label>
                <input type="text" name="description" class="form-control" value="<?php echo $data['description']; ?>" required>
            </div>
            <div class="form-group">
                <label>Numero de Paso</label>
                <input type="text" name="number" class="form-control" value="<?php echo $data['number']; ?>" required>
            </div>
            <input type="hidden" name="id" value="<?php echo  $data['id']; ?>">
            <input type="hidden" name="document_entity_id" value="<?php echo  $document_entity_id; ?>">

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Confirmar</button>
        </div>
    </form>
 <?= $this->endSection() ?>
