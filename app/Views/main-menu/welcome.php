<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>
<!-- Content Header (Page header) -->
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Bienvenido a Perdí Mi Billetera</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <?php if ($_SESSION['rol'] == 'CIUDADANO'){ ?>
    <div class="row" style=" width:95%">
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  <p>Desde esta aplicación vas a poder Gestionar la perdida de la documentación que tengas en tu Billetera.</p>
                  <p>Generar denuncias de extravio y ver los pasos para la renovación. </p>
              </div>
              
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- ./col -->
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                
              </div>
              <div class="card-body">
                <img src="<?= base_url();?>dist/img/inicio.jpg" alt="" style="width:95%">
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- ./col -->
          <!-- ./col -->
        </div>
        <?php }?>
        <!-- /.row -->
    <div class = "card">
      <div class="container-fluid">
        <div class="card-body">
        <!-- Content Header (Page header) -->
        <div></div>
      <div></div>
      </div>
      
    </div>
    </div>
<?= $this->endSection() ?>