<body class="hold-transition dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
  <div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__wobble img-circle elevation-3" src="<?php echo base_url();?>dist/img/wallet.jpg" alt="AdminLTELogo" height="60" width="60">
  </div>

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-dark">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="<?php echo base_url(); ?>" class="nav-link">Home</a>
        </li>
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->
        <li class="nav-item">
          <a class="nav-link" data-widget="navbar-search" href="#" role="button">
            <i class="fas fa-search"></i>
          </a>
          <div class="navbar-search-block">
            <form class="form-inline">
              <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                  <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                  </button>
                  <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>

        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="<?php echo base_url(); ?>dist/img/user1-128x128.jpg" alt="User Avatar"
                  class="img-size-50 mr-3 img-circle">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Brad Diesel
                    <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">Call me whenever you can...</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="<?php echo base_url(); ?>dist/img/user8-128x128.jpg" alt="User Avatar"
                  class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    John Pierce
                    <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">I got your message bro</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="<?php echo base_url(); ?>dist/img/user3-128x128.jpg" alt="User Avatar"
                  class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Nora Silvester
                    <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">The subject goes here</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
          </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span id="span-notification-user" class="badge badge-warning navbar-badge"></span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">Mis notificaciones</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i id="i-user-notification" class="fas fa-envelope mr-2" onclick="viewMyNotifications()"></i>
            </a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt"></i>
          </a>
        </li>
        <!--<li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>-->
        <li class="nav-item dropdown user-menu">
          <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
            <img src="<?= base_url(); ?>dist/img/user2-160x160.jpg" class="user-image img-circle elevation-2"
              alt="User Image">
            <span class="d-none d-md-inline">
              <?= $_SESSION['name'] . ' ' . $_SESSION['lastname']; ?>
            </span>
          </a>
          <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <!-- User image -->
            <li class="user-header bg-primary">
              <img src="<?= base_url(); ?>dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">

            <p>
            <?= $_SESSION['name'].' '.$_SESSION['lastname']; ?>
              <small><?=$_SESSION['rol'];?></small>
            </p>
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
            <?php if($_SESSION['rol'] == 'CIUDADANO'){?>
            <a href="<?= base_url();?>users/profile" class="btn btn-default btn-flat">Perfil</a>
            <?php }?>
            <a href="<?= base_url();?>/users/logout" class="btn btn-default btn-flat float-right">Salir</a>
          </li>
        </ul>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= base_url();?>" class="brand-link">
      <img src="<?php echo base_url();?>dist/img/wallet.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">PMB App</span>
    </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex"></div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
          <!-- SidebarSearch Form 
          <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-sidebar">
                <i class="fas fa-search fa-fw"></i>
              </button>
            </div>
          </div>
          -->
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <!--nav para tipos de documentos-->
            <?php if($_SESSION['id_rol']==1){?>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fa fa-address-card"></i>
                <p>
                  Tipos de Documentos
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?= base_url(); ?>document_types/list" class="nav-link">
                    <i class="fa fa-list nav-icon"></i>
                    <p>Listado</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?= base_url(); ?>document_types/form_add" class="nav-link">
                    <i class="fa fa-plus nav-icon"></i>
                    <p>Nuevo</p>
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-university"></i>
                <p>
                  Entidades
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">

                  <a href="<?= base_url(); ?>Entity_controller/list_entitys" class="nav-link">
                    <i class="fa fa-list nav-icon"></i>
                    <p>Listado</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?= base_url(); ?>Entity_controller/entity_form" class="nav-link">
                    <i class="fa fa-plus nav-icon"></i>
                    <p>Agregar Entidad</p>
                  </a>
                </li>

              </ul>
          
            </li>


            <!--nav para users-->
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>
                  Usuarios
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?= base_url(); ?>users/list" class="nav-link">
                    <i class="fa fa-list nav-icon"></i>
                    <p>Listado</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?= base_url(); ?>users/form_register" class="nav-link">
                    <i class="fa fa-plus nav-icon"></i>
                    <p>Registrar</p>
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item">
              <a href="<?= base_url(); ?>roles/list" class="nav-link">
                <i class="nav-icon fas fa-chalkboard-teacher"></i>
                <p>
                  Roles
                </p>
              </a>
            </li>
            <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-wallet"></i>
                <p>
                Administracion de Denuncias
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
            <ul class="nav nav-treeview">
  
                <li class="nav-item">
                    <a href="<?= base_url();?>complain/list_admin" class="nav-link">
                        <i class="fa fa-list nav-icon"></i>
                        <p>Listado de denuncias </p>
                    </a>
                </li>
            </ul>
            </li>

            <?php }
            else{?>
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-wallet"></i>
                    <p>
                      Mi Billetera
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">

                  <a href="<?= base_url(); ?>documents/list" class="nav-link">
                    <i class="fa fa-list nav-icon"></i>
                    <p>Mi Documentación</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?= base_url(); ?>documents/form_add" class="nav-link">
                    <i class="fa fa-plus nav-icon"></i>
                    <p>Nueva Documentación</p>
                  </a>
                </li>

              </ul>
            </li>
            <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-wallet"></i>
                <p>
                Denuncias
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url();?>complain/form_add" class="nav-link">
                    <i class="nav-icon fas fa-chalkboard-teacher"></i>
                    <p>
                        Realizar Denuncias
                    </p>
                </a>
             </li>
                <li class="nav-item">
                    <a href="<?= base_url();?>complain/list" class="nav-link">
                        <i class="fa fa-list nav-icon"></i>
                        <p>Listado de denuncias </p>
                    </a>
                </li>
            </ul>
            </li>
            
            
            
            
              <?php } ?>


          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <?= $this->renderSection('content'); ?>
    </div>
    <!-- /.content-header -->


    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>

</body>
<script src="<?php echo base_url(); ?>plugins/jquery/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        // Obtén el ID del usuario desde tu sesión PHP
        let userId = <?php echo $_SESSION['id']; ?>;
        // Crea una variable para almacenar el número de notificaciones
        $.ajax({
            url: "<?php echo base_url()?>notifications/users/count",
            method: "GET",
            success: function (data) {
                console.log(JSON.parse(data));
                document.getElementById('span-notification-user').innerHTML = JSON.parse(data);
                let amount = JSON.parse(data)
                if(amount === 0){
                    document.getElementById('i-user-notification').innerHTML = ' No tiene notificaciones' ;
                    document.getElementById('i-user-notification').onclick = null;
                }
                else if(amount === 1){
                    document.getElementById('i-user-notification').innerHTML = ' tiene ' + amount + ' notificación' ;
                }
                else{
                    document.getElementById('i-user-notification').innerHTML = ' tiene ' + amount + ' notificaciones' ;
                }
            }
        });
    });
    function viewMyNotifications(){
        window.location.href = "<?php echo base_url()?>users/profile";
    }
</script>
