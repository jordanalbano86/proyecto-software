<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>
<?php
function load_array($load_array){
  $options = array();
  #$options['selected'] = 'Seleccione una opcion';
  foreach ($load_array as $document) {
      $options[$document['id']] = $document['name'];
  }
  return $options;
}
$states = load_array($states);
$citys = load_array($citys);
 ?>
<div class="c-form-container">
  <div class="container">
    <div class="row">


  <div class="c-form-bottom">
    <div class="col-md-12">
      <?php
      echo form_fieldset('Alta de Entidad',array('class'=>'text-center header'));
      ?>
    <?php
    echo form_open('Entity_controller/add_entity');
    ?>

    <div class="form-group row">
        <label for="c-form-name">
    			<span class="label-text">Nombre de la entidad:</span>
    			<span class="contact-error"></span>
		    </label>
        <?php echo form_input(array('name' => 'name', 'placeholder' => 'Nombre','class'=>'c-form-name form-control')); ?>
    </div>

    <div class="form-group row">
      <label for="c-form-name">
        <span class="label-text">Telefono:</span>
        <span class="contact-error"></span>
      </label>
      <?php echo form_input(array('name' => 'phone', 'placeholder' => 'Telefono','class'=>'c-form-name form-control')); ?>
    </div>
    <div class="form-group row">
      <label for="c-form-name">
        <span class="label-text">Email:</span>
        <span class="contact-error"></span>
      </label>
          <?php echo form_input(array('name' => 'email', 'placeholder' => 'Email','class'=>'c-form-name form-control')); ?>
    </div>
    <div class="form-group row">
      <label for="c-form-name">
        <span class="label-text">Provincia:</span>
        <span class="contact-error"></span>
      </label>
          <?php
          $js = 'id="states" onChange="load_citys();"';
          echo form_dropdown(array('name' => 'states','class'=>'c-form-name form-control'),$states, '', $js);
          
          ?>
          <script src="<?= base_url('js/load_citys.js') ?>"></script>
          
    </div>
    <div class="form-group row">
      <label for="c-form-name">
        <span class="label-text">Ciudad:</span>
        <span class="contact-error"></span>
      </label>
          <?php
          echo form_dropdown(array('name' => 'city','class'=>'c-form-name form-control'), $citys, '', 'id="city"');
          ?>
    </div>
    <div class="form-group row">
      <label for="c-form-name">
        <span class="label-text">Calle:</span>
        <span class="contact-error"></span>
      </label>
          <?php echo form_input(array('name' => 'street', 'placeholder' => 'Calle','class'=>'c-form-name form-control')); ?>
    </div>
    <div class="form-group row">
      <label for="c-form-name">
        <span class="label-text">Numero de Calle:</span>
        <span class="contact-error"></span>
      </label>
          <?php echo form_input(array('name' => 'street_number', 'placeholder' => 'Numero de calle','class'=>'c-form-name form-control')); ?>
    </div>
    <div class="form-group">
      <div class="text-center">
          <?php echo form_submit('guardar', 'Guardar', ['class'=>'btn btn-primary']); ?>
      </div>

    </div>
    
    <?php
    echo form_close();
    ?>
    </div>
  </div>
  </div>
</div>


<?= $this->endSection() ?>
