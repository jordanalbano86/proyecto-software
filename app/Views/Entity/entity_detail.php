<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="<?=base_url('css/entity/list_entitys.css')?>" />
    <title>Tabla de Datos</title>
</head>

    <div class="container">
        <h2>Tabla de Datos</h2>
        <table class="table table-bordered">
          <thead>
              <tr>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Email</th>
                  <th>Calle</th>
                  <th>Provincia</th>
                  <th>Ciudad</th>
                  <th>Telefono</th>
              </tr>
          </thead>
          <tbody id="table-body">

                  <tr>
                      <td><?= $entity[0]['id']; ?></td>
                      <td><?= $entity[0]['name']; ?></td>
                      <td><?= $entity[0]['email']; ?></td>
                      <td><?= $entity[0]['street']; ?></td>
                      <td><?= $entity[0]['province']; ?></td>
                      <td><?= $entity[0]['city']; ?></td>
                      <td><?= $entity[0]['phone']; ?></td>

                  </tr>

          </tbody>
      </table>

    </div>
    <div>
         <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h2>Tipos de Documentos</h2>
          </div><!-- /.col -->

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <div class="card">
        <!-- /.content-header -->
       <div class="container-fluid">
            <div class="card-body">
                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col">Nombre</th><th scope="col">Acci&oacute;n</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($consulta as $tip): ?>
                                <tr>
                                    <td>
                                        <?= $tip['name']; ?>
                                    </td>
                                    <td>

                                      <button type="button" class="btn btn-info" onclick="viewSteps(<?= $tip['id']; ?>)">
                                        <span id="mod" class="material-icons"> search </span>
                                      </button>
                                      <button type="button" class="btn btn-danger" onclick="deleteDocument(<?= $tip['id']; ?>)">
                                        <span id="mod" class="material-icons"> delete </span>
                                      </button>

                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>

                </table>
                <hr>
                <nav>
                    <ul class="l-button">
                      <li>

                         <a class="btn btn-primary" href="<?= base_url();?>document_entity/form_add?id=<?= $entity[0]['id'];?>">Agregar Documento</a>

                      </li>
                    </ul>
                </nav>
            </div>
            </div>
        </div>
    </div>
    <!-- JavaScript para manejar la paginación y cargar datos -->
      <script src="<?= base_url('js/document_entity.js') ?>"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<?= $this->endSection() ?>
