<?= $this->extend('templates/body_admin') ?>

<?= $this->section('content') ?>


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('css/entity/list_entitys.css') ?>" />
</head>

<div>
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Entidades</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Entidades</a></li>
            <li class="breadcrumb-item active">Listado</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

<div class="card">
    <div class="container-fluid">
        <div class="card-body">
            <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Calle</th>
                        <th>Provincia</th>
                        <th>Ciudad</th>
                        <th>Telefono</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody id="table-body">
                    <!-- Datos se cargarán aquí -->
                </tbody>
            </table>
            <hr>
            <nav>
                <ul class="pagination" id="pagination">
                    <!-- Paginación se cargará aquí -->
                </ul>

                <ul class="l-button">
                    <li>
                        <a class="btn btn-primary" href="<?= base_url('Entity_controller/entity_form') ?>">Agregar
                            Entidad</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>


<!-- JavaScript para manejar la paginación y cargar datos -->

<script>
    var entityData = <?= json_encode($entitys) ?>;
</script>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="<?= base_url('js/load_entitys.js') ?>"></script>
<?= $this->endSection() ?>