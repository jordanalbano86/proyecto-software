<?php

namespace App\Controllers;

class Document_steps_controller extends BaseController
{
   protected $db;

  public function __construct(){
  }

  public function form_add()
  {
    if (isset($this->session->loged_in)){
          $this->load_views('document_steps/form_add', [
            'id' => $_GET['id']
          ]);
      }else{
          return redirect()->to('/');
       } 
  }

  public function add()
  {
    if (isset($this->session->loged_in)){
          $steps = $this->steps_model->where('document_entity_id',$_POST['id'])->findAll();
          $number=count($steps)+1;
          $data = array(
              'description' => $_POST['description'],
              'number'=> $number,
              'document_entity_id'=>$_POST['id']
          );

              $this->steps_model->insert($data);
              $steps_new = $this->steps_model->where('document_entity_id',$_POST['id'])->findAll();
              return redirect()->to('document_steps/view_detail?id='.$_POST['id']);
      }else{
          return redirect()->to('/');
      }
   }
   public function form_update()
   {
    if (isset($this->session->loged_in)){
      $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Modificar Paso Recuperacion');
      if (!$permiso){
          $this->load_error_404();
      }else{
            $request = \Config\Services::request();
            $id = $request->getPostGet('id');
            $document = $request->getGet('document');
            # $request = \Config\Services::request();
            $data = $this->steps_model->find($id);

            $this->load_views('document_steps/form_update', [
              'data' => $data,
              'document_entity_id' => $document
            ]);
          }
        }else{
          return redirect()->to('/');
      }
   }

   public function update()
   {
    if (isset($this->session->loged_in)){
          $data = array(
              'description' => $_POST['description'],
              'number' => $_POST['number']
          );
              $this->steps_model->update($_POST['id'], $data);
              return redirect()->to('document_steps/view_detail?id=' . $_POST['document_entity_id']);
      }else{
        return redirect()->to('/');
      }
     }

   public function delete()
   {
    if (isset($this->session->loged_in)){
      $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Eliminar Paso Recuperacion');
      if (!$permiso){
          $this->load_error_404();
      }else{
            $request = \Config\Services::request();
            $id = $request->getPostGet('id');
            $this->steps_model->delete($id);

            return redirect()->back();
      }
    }else{
        return redirect()->to('/');
      }
   }

  public function view_detail() {
    if (isset($this->session->loged_in)){
      $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Detalle Paso Recuperacion');
      if (!$permiso){
          $this->load_error_404();
    }else{
        $request=\Config\Services::request();
        $id = $request->getPostGet('id');
        $steps = $this->steps_model->where('document_entity_id',$id)->orderBy('number', 'asc')->findAll();

        $this->load_views('document_steps/view_detail', [
          'id' => $id,
          'document_steps' => $steps
        ]);
      }
      }else{
        return redirect()->to('/');
      }
}

public function get_steps_by_document_entity() {
  if (isset($this->session->loged_in)){
    $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Detalle Paso Recuperacion');
    if (!$permiso){
        $this->load_error_404();
  }else{
      $request=\Config\Services::request();
      $id = $request->getPostGet('id');
      $steps = $this->steps_model->where('document_entity_id',$id)->orderBy('number', 'asc')->findAll();
      $json= json_encode($steps); // Devuelve los detalles de la entidad en formato JSON
      header('Content-Type: application/json');
      return $this->response->setJSON($json);
      
    }
    }else{
      return redirect()->to('/');
    }
}



}
