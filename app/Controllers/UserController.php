<?php

namespace App\Controllers;


use App\Models\Roles_model;
use CodeIgniter\HTTP\RedirectResponse;
use Config\Services;

class UserController extends BaseController
{

	public function __construct(){
		$this->db = \Config\Database::connect();  // Cargar la base de datos
	}

	public function index()
	{
        $this->load_views('users/list');
	}


    public function list()
	{
        if (isset($this->session->loged_in)){
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Listado Usuarios');
            if (!$permiso){
                $this->load_error_404();
            }else{
            $query = $this->userModel->findAll();
            $this->load_views('users/list', ['users' => $query]);
        }
    }else{
        return redirect()->to('/');
     }
    }


    public function find($id): string
	{
        $this->request->getPost('id');
        return view('list');
    }

    public function form_register()
	{
        if (isset($this->session->loged_in)){
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Registrar Usuario');
            if (!$permiso){
                $this->load_error_404();
            }else{
        $this->load_views('users/form_register');
    }
    }else{
        return redirect()->to('/');
    }
    }

    public function form_update()
    {
        $userInDb = $this->userModel->find($_POST['id']);
        $this->load_views('users/form_update', $userInDb);
    }
    public function register(){
        $data = array(
            'name' => $_POST['name'],
            'lastname' => $_POST['lastname'],
            'username' => $_POST['username'],
            'email' => $_POST['email'],
            'password' => $_POST['password'],
            'role_id' => Roles_model::ID_ROL_USER,
        );
        $repeatePassword = $_POST['password-repeat'];
        $equalsPassword = $this->equalsPassword($repeatePassword, $data['password']);
        if(!$equalsPassword){
            echo '<script language="javascript"> alert("Las contraseñas no coinciden.");history.back();</script>';
        }
        else if(!$this->validEmail($data['email'])){
            echo '<script language="javascript"> alert("El correo electronico no es valido.");history.back()</script>';
        }
        else if($this->existsByUsername($data['username'])){
            echo '<script language="javascript"> alert("El nombre de usuario ya existe.");history.back()</script>';
        }
        else if($this->existsByEmail($data['email'])){
            echo '<script language="javascript"> alert("El correo electronico ya existe.");history.back()</script>';
        }
        else{
            $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
            try {
                $this->userModel->insert($data);
            } catch (\ReflectionException $e) {
                echo '<script language="javascript"> alert("Error al registrar el usuario.");history.back()</script>';
            }
            return redirect()->to('/');
        }

    }


    public function update()
    {
        $data = array(
            'id' => $_POST['id'],
            'name' => $_POST['name'],
            'lastname' => $_POST['lastname'],
            'username' => $_POST['username'],
            'email' => $_POST['email'],
            'password' => $_POST['password'],
            'password-new' => $_POST['password-new'],
            'password-new-repeat' => $_POST['password-new-repeat']
        );
        $userInDb = $this->userModel->find($data['id']);
        if(!$this->thereAreChanges($userInDb, $data) &&  !$this->changePassword($data['password-new'], $data['password-new-repeat'])){
            echo '<script language="javascript"> alert("No se han realizado cambios.");history.back()</script>';
        }
        else if($userInDb['username'] !== $data['username'] && ($this->userModel->where('username', $data['username'])->findAll() > 0)){
            echo '<script language="javascript"> alert("El nombre de usuario ya existe.");history.back()</script>';
        }
        else if($userInDb['email'] !== $data['email'] && ($this->userModel->where('email', $data['email'])->findAll() > 0)){
            echo '<script language="javascript"> alert("El correo electronico ya existe.");history.back()</script>';
        }
        else if (!$this->equalsPassword($data['password-new'], $data['password-new-repeat'])) {
            echo '<script language="javascript"> alert("Las contraseñas nuevas no coinciden.");history.back()</script>';
        }
        else if (!password_verify($data['password'], $this->userModel->find($data['id'])['password']) ) {
            echo '<script language="javascript"> alert("Contraseña incorrecta.");history.back()</script>';

        } else if ($this->changePassword($data['password-new'], $data['password-new-repeat']) && $this->passwordEqualsToNewPassword($data['password'], $data['password-new'], $data['password-new-repeat'])) {
            echo '<script language="javascript"> alert("La nueva contraseña no puede ser igual a la anterior.");history.back()</script>';

        }
        else{
            if($this->changePassword($data['password-new'], $data['password-new-repeat'])){
                $data['password'] = password_hash($data['password-new'], PASSWORD_BCRYPT);
            }
            try {
                $this->userModel->update($data['id'], $data);
            } catch (\ReflectionException $e) {
            }

            return redirect()->to('users/list');
        }
    }

    public function update_citizen()
    {
        $data = array(
            'id' => $_POST['id'],
            'name' => $_POST['name'],
            'lastname' => $_POST['lastname'],
            'username' => $_POST['username'],
            'email' => $_POST['email'],
        );
        $id = $_POST['id'];
        $rules = [
            'name' => [
                'label' => 'nombre',
                'rules' => 'required',
                'errors'=> 
                    [
                        'required'=> 'El {field} es obligatorio',
                    ],
                ],
            'lastname' => [
                'label' => 'apellido',
                'rules' => 'required',
                'errors'=> 
                    [
                        'required'=> 'El {field} es obligatorio',                        ],
                    ],
            'username' => [
                'label' => 'nombre de usuario',
                'rules' => "required|is_unique[users.username, id, {$id}]",
                'errors'=> 
                    [
                        'required'=> 'El {field} es obligatorio',
                        'is_unique' => 'El {field} ya existe. ',
                    ],
                ],
                'email' => [
                    'label' => 'email',
                    'rules' => "required|is_unique[users.email, id, {$id}]",
                    'errors'=> 
                        [
                            'required'=> 'El {field} es obligatorio',
                            'is_unique' => 'El {field} ya existe. ',
                        ],
                    ],
        ];
            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            }else{
            try {
                $this->userModel->update($data['id'], $data);
            } catch (\ReflectionException $e) {
            }
            if ($this->session->id == $_POST['id']){
                $data = [
                    'id' => $this->session->id,
                    'name' => $_POST['name'],
                    'lastname' => $_POST['lastname'],
                    'email' => $_POST['email'],
                    'username' => $_POST['username'],
                    'id_rol' => $this->session->id_rol,
                    'rol' => $this->session->rol,
                    'loged_in' => true
                ];
                $this->session->set($data);
            }
            if ($this->session->rol == 'CIUDADANO'){
                return redirect()->to('users/profile');
            }
            return redirect()->to('users/list');
        }
    }

    public function update_password()
    {
        $data = array(
            'id' => $_POST['id'],
            'name' => $this->session->name,
            'lastname' => $this->session->lastname,
            'username' => $this->session->username,
            'email' => $this->session->email,
            'password' => $_POST['password'],
            'password-new' => $_POST['password-new'],
            'password-new-repeat' => $_POST['password-new-repeat']
        );
        $userInDb = $this->userModel->find($data['id']);
        if(!$this->thereAreChanges($userInDb, $data) &&  !$this->changePassword($data['password-new'], $data['password-new-repeat'])){
            echo '<script language="javascript"> alert("No se han realizado cambios.");history.back()</script>';
        }
        else if($userInDb['username'] !== $data['username'] && ($this->userModel->where('username', $data['username'])->findAll() > 0)){
            echo '<script language="javascript"> alert("El nombre de usuario ya existe.");history.back()</script>';
        }
        else if($userInDb['email'] !== $data['email'] && ($this->userModel->where('email', $data['email'])->findAll() > 0)){
            echo '<script language="javascript"> alert("El correo electronico ya existe.");history.back()</script>';
        }
        else if (!$this->equalsPassword($data['password-new'], $data['password-new-repeat'])) {
            echo '<script language="javascript"> alert("Las contraseñas nuevas no coinciden.");history.back()</script>';

        } else if ($this->changePassword($data['password-new'], $data['password-new-repeat']) && $this->passwordEqualsToNewPassword($data['password'], $data['password-new'], $data['password-new-repeat'])) {
            echo '<script language="javascript"> alert("La nueva contraseña no puede ser igual a la anterior.");history.back()</script>';

        }
        else{
            if($this->changePassword($data['password-new'], $data['password-new-repeat'])){
                $data['password'] = password_hash($data['password-new'], PASSWORD_BCRYPT);
            }
            try {
                $this->userModel->update($data['id'], $data);
                $this->session->destroy();
                return redirect()->to('/');
            } catch (\ReflectionException $e) {
            }
        }
    }

    public function delete()
	{
        $this->userModel->delete($_POST['id']);
        return redirect()->to('users/list');
    }

    private function equalsPassword($repeatePassword, $password): bool
    {
        return $repeatePassword === $password;
    }

    private function passwordEqualsToNewPassword($password, $passwordNew, $passwordNew2): bool
    {
        return $passwordNew === $password || $passwordNew2 === $password;
    }

    private function changePassword($passwordNew, $passwordNew2)
    {
        return ($passwordNew !== null || $passwordNew2 !== null);
    }

    private function thereAreChanges(array $userInDb, array $userToSave)
    {
        return $userInDb['name'] !== $userToSave['name'] ||
            $userInDb['lastname'] !== $userToSave['lastname'] ||
            $userInDb['username'] !== $userToSave['username'] ||
            $userInDb['email'] !== $userToSave['email'] ||
            password_verify($userToSave['password'], $userInDb['password']);
    }


    public function logout(){
        $this->session->destroy();
        return redirect()->to('/');
    }

    public function login()
    {
        $emailOrUsername = $this->request->getPost('email');
        $password = $this->request->getPost('password');
        $result = $this->userModel->where('email', $emailOrUsername)->first();
        if ($result) {
            echo $result['role_id'];
            if (password_verify($password, $result['password'])) {
                $rol = $this->rol_model->find($result['role_id']);
                $data = [
                    'id' => $result['id'],
                    'name' => $result['name'],
                    'lastname' => $result['lastname'],
                    'email' => $result['email'],
                    'username' => $result['username'],
                    'id_rol' => $result['role_id'],
                    'rol' => $rol['name'],
                    'loged_in' => true
                ];
                $this->session->set($data);
                $this->session->setFlashdata('msg', 'Bienvenido ' . $result['name']);
                $this->session->setFlashdata('alert', 'alert-success');
                return redirect()->to('/');
            } else {
                echo '<script language="javascript"> alert("Contraseña incorrecta.");history.back();</script>';
            }
        } else {
            echo '<script language="javascript"> alert("El usuario no se encontro");history.back();</script>';
        }
    }

    public function load_profile(){
        if (isset($this->session->loged_in)){
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Perfil Ciudadano');
            if (!$permiso){
                $this->load_error_404();
            }else{
                $this->load_views('users/profile');
            }
            }else{
                return redirect()->to('/');
            }
    }



}
