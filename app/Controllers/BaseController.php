<?php

namespace App\Controllers;

use App\Models\Document_model;
use App\Models\Notification_model;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

use App\Models\Roles_model;
use App\Models\Permits_model;
use App\Models\Roles_Permits_model;
use App\Models\Document_types_model;
use App\Models\UserModel;
use Config\Services;

use App\Models\Entity_model;
use App\Models\City_model;
use App\Models\Document_entity_model;
use App\Models\Direction_model;
use App\Models\Province_model;
use App\Models\Document_steps_model;

use App\Models\Complain_model;
use App\Models\Documents_Complain_model;
use CodeIgniter\Database\Exceptions\DatabaseException;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */
abstract class BaseController extends Controller
{
    /**
     * Instance of the main Request object.
     *
     * @var CLIRequest|IncomingRequest
     */
    protected $request;
    public $documents_complain_model, $complain_model, $rol_model, $permits_model, $rolesPermits_model, $session, $tipoD, $userModel, $province_model,
    $city_model, $direction_model, $entity_model, $doc_entity, $steps_model, $notification_model, $document_model;

    /**
     * An array of helpers to be loaded automatically upon
     * class instantiation. These helpers will be available
     * to all other controllers that extend BaseController.
     *
     * @var array
     */
    protected $helpers = ['form'];

    /**
     * Be sure to declare properties for any property fetch you initialized.
     * The creation of dynamic property is deprecated in PHP 8.2.
     */
    // protected $session;

    /**
     * @return void
     */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        // Preload any models, libraries, etc, here.
        $this->db = \Config\Database::connect();
        $this->session = \Config\Services::session();
        $this->tipoD = new Document_types_model();
        $this->userModel = new UserModel();
        $this->rol_model = new Roles_model();
        $this->permits_model = new Permits_model();
        $this->rolesPermits_model = new Roles_Permits_model();
        $this->province_model= new Province_model();
        $this->city_model=new City_model();
        $this->direction_model=new Direction_model();
        $this->entity_model=new Entity_model();
        $this->doc_entity = new Document_entity_model();
        $this->steps_model = new Document_steps_model();
        $this->document_model= new Document_model();
        $this->complain_model = new Complain_model();
        $this->documents_complain_model= new Documents_Complain_model();
        $this->notification_model = new Notification_model();
    }

    public function load_views($view, $data = []){
        echo view('templates/header');
        echo view($view, $data);
        echo view('templates/footer');
    }

    public function load_error_404(){
        $message = 'No tiene permiso de acceso. Comuniquese con el Administrador.';
        echo view('errors/html/error_404', ['message'=>$message]);
    }

    public function existsByUsername($username): bool
    {
        $user = $this->userModel->where('username', $username)->findAll();
        return count($user) > 0;
    }

    public function existsByEmail($email): bool
    {
        $user = $this->userModel->where('email', $email)->findAll();
        return count($user) > 0;
    }
    public function validEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}
