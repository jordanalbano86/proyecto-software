<?php

namespace App\Controllers;

class City_controller extends BaseController
{
   protected $db;

  public function __construct(){
  }

  public function get_cities() {

    $request=\Config\Services::request();

    // Recibe el id_provincia del estado seleccionado desde la solicitud POST
    $province_id = $request->getPostGet('id');

    $cities = $this->city_model->where('province_id', $province_id)->findAll();

    $json = json_encode($cities);
    header('Content-Type: application/json');
    // Devuelve las ciudades en formato JSON
    return $this->response->setJSON($json);
}



}
