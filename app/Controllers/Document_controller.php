<?php

namespace App\Controllers;

use App\Models\Document_model;
use App\Models\Document_types_model;
use App\Models\Document_entity_model;

class Document_controller extends BaseController
{
    protected $helpers = ['form'];



    public function list()
    {
        if (isset($this->session->loged_in)) {
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol, 'Listado Mi Documentacion');
            if (!$permiso) {
                $this->load_error_404();
            } else {

                $data['data'] = $this->document_model->find_documents_by_user($_SESSION['id']); //usuario que esta logueado

                $this->load_views('documents/list', $data);
            }
        } else {
            return redirect()->to('/');
        }


    }

    public function delete($id)
    {
        if (isset($this->session->loged_in)) {
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol, 'Eliminar Documentacion');
            if (!$permiso) {
                $this->load_error_404();
            } else {
                $this->document_model->delete($id);
                return redirect()->to('documents/list');
            }
        } else {
            return redirect()->to('/');
        }



    }

    public function form_add()
    {
        $data['data'] = $this->tipoD->findAll();
        // var_dump($data);`
        $this->load_views('documents/form_add', $data);

    }


    public function new()
    {
        // var_dump($_POST);
        // die;

        if (isset($this->session->loged_in)) {
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol, 'Agregar Documentacion');
            if (!$permiso) {
                $this->load_error_404();

            } else {
                $rules = [
                    'number' => [
                        'label' => '"Número"',
                        'rules' => 'required',
                        'errors' =>
                            ['required' => 'El campo {field} es obligatorio']
                    ],
                    'expiration_date' => [
                        'rules' => 'isLater[date_of_issue]',
                        'errors' => [
                            'isLater' => 'La fecha de expiración debe ser posterior a la fecha de emisión',
                        ],
                    ],
                    'date_of_issue' => [
                        'label' => '"Fecha de emisión"',
                        'rules' => 'required',
                        'errors' => [
                            'required' => 'El campo {field} es obligatorio',
                        ],
                    ],
                    'entity_id' => [
                        'label' => '"Entidad Emisora"',
                        'rules' => 'required',
                        'errors' => [
                            'required' => 'Complete el campo {field}',
                        ],
                    ]

                ];


                //validar que la fecha de vencimiento sea posterior a la fecha de emision
                //validar que document_type_id y entity_id no vengan vacios por post
                //validar que el numero no venga vacio en el post 


                if (!$this->validate($rules)) {
                    return redirect()->back()->withInput();
                }



                $documents_entities['document_entity'] = $this->doc_entity->get_entities_by_document_id($_POST['entity_id'], $_POST['document_type_id']);

                $data = array(
                    'user_id' => $_SESSION['id'],
                    //acá va el id del usuario que este logueado
                    'number' => $_POST['number'],
                    'expiration_date' => $_POST['expiration_date'],
                    'date_of_issue' => $_POST['date_of_issue'],
                    'document_entity_id' => $documents_entities['document_entity'][0]['id'],
                    'activated' => '1',
                );


                $this->document_model->insert($data);
                return redirect()->to('documents/list');
            }
        } else {
            return redirect()->to('/');
        }

    }




    public function form_update($document_id)
    {
        $data['documents'] = $this->document_model->find_document($document_id);
        $type_doc = $data['documents'][0]['document_type_id'];
        $data['entities'] = $this->doc_entity->get_entities_by_document_type($type_doc);
        // var_dump($data);
        // die;
        $this->load_views('documents/form_update', $data);

    }

    public function update()
    {
        // var_dump($request);
        // die;
        if (isset($this->session->loged_in)) {
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol, 'Modificar Documentacion');
            if (!$permiso) {
                $this->load_error_404();
            } else {
                $rules = [
                    'number' => [
                        'label' => '"Número"',
                        'rules' => 'required',
                        'errors' =>
                            ['required' => 'El campo {field} es obligatorio']
                    ],
                    'expiration_date' => [
                        'rules' => 'isLater[date_of_issue]',
                        'errors' => [
                            'isLater' => 'La fecha de expiración debe ser posterior a la fecha de emisión',
                        ],
                    ],
                    'date_of_issue' => [
                        'label' => '"Fecha de emisión"',
                        'rules' => 'required',
                        'errors' => [
                            'required' => 'El campo {field} es obligatorio',
                        ],
                    ],
                    'entity_id' => [
                        'label' => '"Entidad Emisora"',
                        'rules' => 'required',
                        'errors' => [
                            'required' => 'Complete el campo {field}',
                        ],
                    ]

                ];

                if (!$this->validate($rules)) {
                    return redirect()->back()->withInput();
                }




                $documents_entities['document_entity'] = $this->doc_entity->get_entities_by_document_id($_POST['entity_id'], $_POST['document_type_id']);


                $data = array(
                    'number' => $_POST['number'],
                    'expiration_date' => $_POST['expiration_date'],
                    'date_of_issue' => $_POST['date_of_issue'],
                    'document_entity_id' => $documents_entities['document_entity'][0]['id'],
                );


                $this->document_model->update($_POST['id'], $data);
                return redirect()->to('documents/list');
            }
        } else {
            return redirect()->to('/');
        }

    }


}
