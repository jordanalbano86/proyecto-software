<?php

namespace App\Controllers;

class Roles_controller extends BaseController
{
    


    public function form_add()
    {
        if (isset($this->session->loged_in)){
        $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Agregar Rol');
        if (!$permiso){
            $this->load_error_404();
        }else{
            $this->load_views('roles/form_add');
        }
        }else{
            return redirect()->to('/');
        }
    }

    public function add()
    {
        $rules = [
            'name' =>[
                'label'=> 'nombre',
                'rules' => 'required|is_unique[roles.name]',
                'errors'=> 
                    [
                        'required'=> 'El {field} del rol es obligatorio',
                        'is_unique' => 'El {field} del rol ya existe. ',
                    ],
            ],
        ];

        if (isset($this->session->loged_in)){
            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            }else{
            $this->rol_model->insert(['name' => $_POST['name']]);
            return redirect()->to('roles/list');
            }
        }else{
            return redirect()->to('/');
        }
     }


    public function delete()
    {
        if (isset($this->session->loged_in)){
        $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Eliminar Rol');
        if (!$permiso){
            $this->load_error_404();
        }else{
            $this->rol_model->delete($_POST['id']);
            return redirect()->to('roles/list');
        }
        }else{
            return redirect()->to('/');
        }
    }

    public function form_update($id)
    {
        if (isset($this->session->loged_in)){
            $rol = $this->rol_model->find($id);
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Modificar Rol');
            if (!$permiso){
                $this->load_error_404();
            }else{
                $this->load_views('roles/form_update', ['rol'=>$rol]);
                }
            
        }else{
            return redirect()->to('/');
        }
    }

    public function update()
    {
        $id = $_POST['id'];
        $rules =[
            'name' =>[
                'label'=> 'nombre',
                'rules' => "required|is_unique[roles.name, id, $id]",
                'errors'=> 
                    [
                        'required'=> 'El {field} del rol es obligatorio',
                        'is_unique' => 'El {field} del rol ya existe. ',
                    ],
            ],
        ];
        if (isset($this->session->loged_in)){
            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            }else{
            $this->rol_model->update($_POST['id'], ['name' => $_POST['name']]);
            return redirect()->to('roles/list');
            }
        }else{
            return redirect()->to('/');
        }
      }

      public function list()
      {
        $roles = $this->rol_model->findAll();
        if (isset($this->session->loged_in)){
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Gestion Roles');
        if (!$permiso){
            $this->load_error_404();
        }else{
            $this->load_views('roles/list',['roles'=>$roles]);
        }
        }else{
           return redirect()->to('/');
        }
       }

       public function list_for_rol()
      {
        if (isset($this->session->loged_in)){
        $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Permisos Rol');
         
        if (!$permiso){
            $this->load_error_404();
        }else{
        $permisos = $this->permits_model->findAll();
        $permisosAsign = $this->rolesPermits_model->where('roles_id', $_POST['id'])->findAll();
        $datos = array();

        foreach ($permisosAsign as $permisoAsign){
            $datos[$permisoAsign['permits_id']] = true;
        }
        $this->load_views('roles/list_permits',[
              'permisos'=>$permisos,
              'id_rol' => $_POST['id'],
              'asignado' => $datos
            ]); 
        }
        }else{
            return redirect()->to('/');
        }

       }

       public function add_permits()
       {
        if (isset($this->session->loged_in)){
            $idRol = $_POST['id_rol'];
            $permisos = $_POST['permisos'];

            $this->rolesPermits_model->where('roles_id', $idRol)->delete();

            foreach ($permisos as $permiso)
            {
                $this->rolesPermits_model->save(['roles_id' => $idRol, 'permits_id' => $permiso]);
            }
        return redirect()->to('roles/list');
        }else{
            return redirect()->to('/');
         }
       }

       public function form_permits_add(){
        if (isset($this->session->loged_in)){
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Nuevo Permiso');
        if (!$permiso){
            $this->load_error_404();
        }else{
            $this->load_views('roles/form_permits_add');
        }}else{
            return redirect()->to('/');
        }
       }

       public function new_permits()
    {
        $rules = [
            'name' =>[
                'label'=> 'nombre',
                'rules' => 'required|is_unique[permits.name]',
                'errors'=> 
                    [
                        'required'=> 'El {field} del permiso es obligatorio',
                        'is_unique' => 'El {field} del permiso ya existe. ',
                    ],
            ],
        ];

        if (isset($this->session->loged_in)){
            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            }else{
                $this->permits_model->insert(['name' => $_POST['name'], 'level' => 2]);
                return redirect()->to('roles/list');
            }
        }else{
            return redirect()->to('/');
        }
     }

}
