<?php

namespace App\Controllers;
use App\Models\Document_entity_model;
use App\Models\Entity_model;
class Document_types_controller extends BaseController
{



    public function form_add()
    {
        if (isset($this->session->loged_in)){
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Agregar Tipo Documento');
            if (!$permiso){
                $this->load_error_404();
            }else{
            $this->load_views('document_types/form_add');
        }
        }else{
            return redirect()->to('/');
        }
    }

    public function add()
    {
        $rules =[
            'name' =>[
                'label'=> 'nombre',
                'rules' => 'required|is_unique[document_types.name]',
                'errors'=> 
                    [
                        'required'=> 'El {field} del tipo de documento es obligatorio',
                        'is_unique' => 'El {field} del tipo de documento ya existe. ',
                    ],
            ],
        ];
        if (isset($this->session->loged_in)){
            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            }else{
            $data = array(
                'name' => $_POST['name']
            );
            $this->tipoD->insert($data);
            return redirect()->to('document_types/list');
        }
        }else{
            return redirect()->to('/');
        }


     }


    public function delete()
    {
        if (isset($this->session->loged_in)){
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Eliminar Tipo Documento');
            if (!$permiso){
                $this->load_error_404();
            }else{
        $this->tipoD->delete($_POST['id']);
        return redirect()->back();
            }
        }else{
            return redirect()->to('/');
        }
    }

    public function form_update($id)
    {
        if (isset($this->session->loged_in)){
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Modificar Tipo Documento');
            if (!$permiso){
                $this->load_error_404();
            }else{
            $consulta = $this->tipoD->find($id);
            $this->load_views('document_types/form_update', ['consulta'=>$consulta]);
        }
        }else{
            return redirect()->to('/');
        }
    }

    public function update()
    {
        $id = $_POST['id'];
        $rules =[
            'name' =>[
                'label'=> 'nombre',
                'rules' => "required|is_unique[document_types.name, id, $id]",
                'errors'=> 
                    [
                        'required'=> 'El {field} del tipo de documento es obligatorio',
                        'is_unique' => 'El {field} del tipo de documento ya existe. ',
                    ],
            ],
        ];
        if (isset($this->session->loged_in)){
            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            }else{
            $data = array(
                'name' => $_POST['name'],
            );
            $this->tipoD->update($_POST['id'], $data);
            return redirect()->to('document_types/list');
        }
        }else{
           return redirect()->to('/');
        }

      }

      public function list()
      {
        if (isset($this->session->loged_in)){
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Listado Tipos Documento');
            if (!$permiso){
                $this->load_error_404();
            }else{
            $data = $this->tipoD->findAll();
            $this->load_views('document_types/list',['data' => $data]);
            }
        }else{
            return redirect()->to('/');
         } 

       }


}
