<?php

namespace App\Controllers;
use App\Models\Document_types_model;
use App\Models\Document_entity_model;
use App\Models\Entity_model;
class Document_entity_controller extends BaseController
{

    // public function list()
    // {
    //     $request = \Config\Services::request();
    //     $id = $request->getGet('id');
    //     $tipoD = new Document_types_model();
    //     $consulta = $tipoD->where('entity_id',$id)->findAll();
    //     $tipos = array('consulta' => $consulta);
    //     return view('Entity/entity_detail', $tipos);
    // }

    public function form_add()
    {
        if (isset($this->session->loged_in)){
            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Agregar Tipo Documento a Entidad');
            if (!$permiso){
                $this->load_error_404();
            }else{
                $request = \Config\Services::request();
                $id = $request->getGet('id');
                $document_types = $this->tipoD->findAll();
                $document_entity= $this->doc_entity->where('entity_id',$id)->findAll();

                $excludedIds = array_column($document_entity, 'document_type_id');

                // Filtrar $document_types para excluir elementos con ids coincidentes
                $filteredDocumentTypes = array_filter($document_types, function ($type) use ($excludedIds) {
                    return !in_array($type['id'], $excludedIds);
                });

                $this->load_views('document_entity/form_add', [
                'id' => $id,
                'document_types' => $filteredDocumentTypes
                ]);
            }
        }else{
            return redirect()->to('/');
         } 
    }

    public function add()
    {
        if (isset($this->session->loged_in)){
        $data = array(
            'document_type_id' => $_POST['document_type_id'],
            'entity_id'=>$_POST['id']
        );

            $this->doc_entity->insert($data);
            return redirect()->to('Entity_controller/list_entity_with_documents_type?id=' . $data['entity_id']);
        }else{
            return redirect()->to('/');
         } 
     }


    public function delete()
    {
        if (isset($this->session->loged_in)){

            $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Quitar Tipo Documento a Entidad');
            if (!$permiso){
                $this->load_error_404();
            }else{
            $id = $_GET['id'];
            $this->doc_entity->delete($id);
            return redirect()->back();
            }
        }else{
            return redirect()->to('/');
         } 
    }

    public function form_update()
    {
                $consulta = $this->tipoD->find($_GET['id']);
                $this->load_views('document_entity/form_update', [
                'consulta'=>$consulta,
                'entity_id'=>$_GET['entity_id']
                ]);
         
    }

    public function update()
    {
        $data = array(
            'name' => $_POST['name'],
            'entity_id'=>$_POST['entity_id']
        );
            $this->tipoD->update($_POST['id'], $data);
            $entity= $this->entity_model->getOne($_POST['entity_id']);
            $consulta = $this->tipoD->where('entity_id',$_POST['entity_id'])->findAll();

            $this->load_views('Entity/entity_detail',[
              'consulta' => $consulta,
              'entity' =>$entity
              ] );
      }

//se agrega para recuperar las entidades que pueden generar el tipo de doc pasado por parametros 
public function list_entities_by_document_type($document_type_id){
    $document_entity_model = new Document_entity_model();
    $data['entities']= $document_entity_model->get_entities_by_document_type($document_type_id);
    // var_dump($data);
    // die;
    
    // Devolver el array como JSON
    header('Content-Type: application/json');
    echo json_encode($data);

}

}
