<?php

namespace App\Controllers;

class Complains_controller extends BaseController
{
   protected $db;

  public function __construct(){
    helper('form');
  }

  public function form_complain(){
    if (isset($this->session->loged_in)){
      $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Agregar Denuncia');
      if (!$permiso){
         $this->load_error_404();
      }else{
           $wallet = $this->document_model->find_documents_by_user($this->session->id);

           $this->load_views('/Complain/form_add', [
                        'wallet' => $wallet
                    ]);
      }
    }else{
        return redirect()->to('/');
    }
  }

  public function add_complain(){
    if (isset($this->session->loged_in)){
      $request = \Config\Services::request();
      $this->db->transStart();
      $documents = json_decode($request->getBody(), true);
      try {
          $complain = array(
              'start_date' => date('Y-m-d H:i:s'),
              'end_date' => NULL,
              'modification_date' =>date('Y-m-d H:i:s'),
              'status_id' =>1, //ID del estado iniciado - Cambiar por constante
          );
          # Inserta la dirección
          $this->complain_model->insert($complain);
          $complain_id = $this->complain_model->getInsertID();

          $all_documents_complains = array();
          foreach ($documents['datosSeleccionados'] as $value) {
            $id = $value['document_id'];
            $data = ['activated'=> false];
            $this->document_model->update($id,$data);
            $documents_complains = array(
                'complain_id' => $complain_id,
                'document_id' =>$id
            );
           
            array_push($all_documents_complains,$documents_complains);
          }
            $this->documents_complain_model->insertBatch($all_documents_complains);
            $this->db->transComplete();
            if ($this->db->transStatus() === false) {
              return $this->response->setJSON(['success' => false, 'message' => 'Hubo un problema al generar la denuncia']);
            }
            else {
                $notificationController = new Notification_controller();
                $notificationController->sendNotification($this->session->id,'Se ha generado una nueva denuncia',1);
              // GENERAR NOTIFICACION AL USUARIO
              return $this->response->setJSON(['success' => true, 'message' => 'La denuncia se genero con exito']);
            }
          } catch (Exception $e) {
          // Rollback en caso de error
            // Maneja el error, puedes agregar registro de errores, imprimir un mensaje, etc.
            echo 'Error: ' . $e->getMessage();
          }
        }else{
          return redirect()->to('/');
      }
  }

  public function list_complains()
  {
    if (isset($this->session->loged_in)){
      $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Listado Denuncia Ciudadano');
      if (!$permiso){
        $this->load_error_404();
      }else{
          $complains=$this->complain_model->find_all_by_user_id($this->session->id);
          $this->load_views('Complain/list',[
            'complains' => $complains
            ] );
        } 
      }else{
        return redirect()->to('/');
      }
  }
  public function view_detail()
  {
    if (isset($this->session->loged_in)){
      $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Ver Detalle de Denuncia');
      if (!$permiso){
        $this->load_error_404();
      }else{
          $id=$_GET['id'];
          $complain=$this->complain_model->get($id);
          $documents=$this->documents_complain_model->find_all_by_complain_id($id);
         
          
          $this->load_views('Complain/view_detail',[
            'complain' => $complain,
            'documents' => $documents            
            ] );
        } 
      }else{
        return redirect()->to('/');
      }
  }

  


  public function list_complains_admin()
  {
    if (isset($this->session->loged_in)){
      $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Listado Denuncia Admin');
      if (!$permiso){
        $this->load_error_404();
      }else{
          $complains=$this->complain_model->find_all();
          $this->load_views('Complain/list_admin',[
            'complains' => $complains
            ] );
        } 
      }else{
        return redirect()->to('/');
      }
  }

  public function init_complain()
  {
    if (isset($this->session->loged_in)){

      $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Administrar Denuncia');
      if (!$permiso){
        $this->load_error_404();
      }else{
          $request = \Config\Services::request();
          $requestData = $this->request->getJSON(true);
          $id = $requestData['id'];
          $complain=$this->complain_model->find($id);

          if($complain['status_id'] === "1"){

            $this->complain_model->update($id,['status_id' =>2,'end_date'=>date('Y-m-d H:i:s')]);
            $document_complain = $this->documents_complain_model->where('complain_id',$id)->first();
            $document=$this->document_model->find($document_complain['document_id']);
            $notificationController = new Notification_controller();
            $notificationController->sendNotification($this->session->id,'Tu documentacion cambio de estado',$document['user_id']);
            
            return $this->response->setJSON(['success' => true, 'message' => 'La denuncia fue iniciada']);

          }
          else{
            return $this->response->setJSON(['success' => false, 'message' => 'La denuncia ya esta iniciada']);
          }
          

          
      }
  }
  else{
    return redirect()->to('/');
  }
}

public function term_complain()
  {
    if (isset($this->session->loged_in)){
      $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Administrar Denuncia');
      if (!$permiso){
        $this->load_error_404();
      }else{
          $request = \Config\Services::request();
          $requestData = $this->request->getJSON(true);
          $id = $requestData['id'];
          $complain=$this->complain_model->find($id);

          if($complain['status_id']=== "2"){

            $this->complain_model->update($id,['status_id' =>3,'end_date'=>date('Y-m-d H:i:s')]);
            $document_complain = $this->documents_complain_model->where('complain_id',$id)->first();
            $document=$this->document_model->find($document_complain['document_id']);
            $notificationController = new Notification_controller();
            $notificationController->sendNotification($this->session->id,'La denuncia fue finalizada, por favor vaya a retirar su documentacion',$document['user_id']);
            
            return $this->response->setJSON(['success' => true, 'message' => 'La denuncia fue finalizada']);
          }
          else{
            return $this->response->setJSON(['success' => false, 'message' => 'Hubo un error al finalizar la denuncia']);
          }



      }
  }
  else{
    return redirect()->to('/');
  }
}




}
