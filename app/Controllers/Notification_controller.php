<?php

namespace App\Controllers;
use App\Models\Notification_model;
use App\Models\UserModel;

class Notification_controller extends BaseController
{

    public function sendNotification($transmitter, $message,$receiver) {

            $data = [
                'transmitter' => $transmitter,
                'receiver' => $receiver,
                'message' => $message,
                'created_on' => date('Y-m-d H:i:s'),
                'read' => 0
            ];
            $notification_model = new Notification_model();
        try {
            $notification_model->insert($data);
        } catch (\ReflectionException $e) {

        }
    }

    public function getNotificationOfUser() {
        $notification_model = new Notification_model();
        $data = $notification_model->where('receiver', $this->session->id)->findAll();
        $userOfNotification = new UserModel();
        $i = 1;
        foreach ($data as $key => $notification) {
            $receiverUser = $userOfNotification->find($notification['receiver']);
            $transmitterUser = $userOfNotification->find($notification['transmitter']);
            $data[$key]['notification_number'] = $i;
            $data[$key]['receiver_details'] = $receiverUser;
            $data[$key]['transmitter_details'] = $transmitterUser;
            $i++;
        }
        return json_encode($data);
    }
    public function read() {
        $notification_model = new Notification_model();
        $user = $notification_model->find($_POST['id']);
        $data = array(
            'id' => $user['id'],
            'transmitter' => $user['transmitter'],
            'receiver' => $user['receiver'],
            'message' => $user['message'],
            'created_on' => $user['created_on'],
            'read' => '1'
        );
        try {
            $bool = $notification_model->update($_POST['id'], $data);
        } catch (\ReflectionException $e) {
            return json_encode($e);
        }
        return json_encode($data);
    }

    public function getAmountOfNotificationOfUser() {
        $notification_model = new Notification_model();
        $data = $notification_model->where('receiver', $this->session->id)->where('read', 0)->findAll();
        return json_encode(count($data));
    }

}
