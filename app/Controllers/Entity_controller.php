<?php

namespace App\Controllers;

class Entity_controller extends BaseController
{
   protected $db;

  public function __construct(){
    helper('form');
  }

  public function create_entity(){
    if (isset($this->session->loged_in)){
      $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Agregar Entidad');
      if (!$permiso){
         $this->load_error_404();
      }else{
          $states = $this->province_model->findAll();
          $citys = $this->city_model->where('province_id',reset($states)['id'])->findAll();

          $this->load_views('/Entity/Create_entity_form', [
                        'citys' => $citys,
                        'states' =>$states
                    ]);
      }
    }else{
        return redirect()->to('/');
    } 
  }

  public function add_entity(){
    if (isset($this->session->loged_in)){
      try {
          $direccion = array(
              'city_id' => $_POST['city'],
              'street' => $_POST['street'],
              'number' => $_POST['street_number'],
          );
          # Inserta la dirección
          $this->direction_model->insert($direccion);
          $id_direccion = $this->direction_model->getInsertID();

          $entidad = array(
              'name' => $_POST['name'],
              'phone' => $_POST['phone'],
              'email' => $_POST['email'],
              'direction_id' => $id_direccion
          );
          # Inserta la entidad
          $result = $this->entity_model->insert($entidad);
          return redirect()->to('../Entity_controller/list_entitys');
          } catch (Exception $e) {
          // Rollback en caso de error
            // Maneja el error, puedes agregar registro de errores, imprimir un mensaje, etc.
            echo 'Error: ' . $e->getMessage();
          }
        }else{
          return redirect()->to('/');
      } 

  }

  public function list_entitys(){
    if (isset($this->session->loged_in)){
      $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Listado Entidades');
      if (!$permiso){
          $this->load_error_404();
      }else{
        // En tu modelo de entidad (Entity_model) o en el controlador
        $entitys = $this->entity_model->getAll();
        $this->load_views('/Entity/entity_list', [
                      'entitys' =>$entitys
                  ]);
      }
    }else{
      return redirect()->to('/');
  } 
  }

  public function delete_entity() {
    if (isset($this->session->loged_in)){
      $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Eliminar Entidad');
      if (!$permiso){
        $this->load_error_404();
      }else{
          try {
              // Recibe el id_provincia del estado seleccionado desde la solicitud POST
              $entity = $this->entity_model->find($_GET['id']);

              if ($entity && isset($entity['direction_id'])) {
                  $entitys = $this->entity_model->delete($_GET['id']);
                  $direction = $this->direction_model->delete($entity['direction_id']);

                  //$direction=$direction_model->delete($id_direction);
                  return redirect()->to('../Entity_controller/list_entitys');
              } else {
                  return "No se encontró la entidad o la dirección asociada.";
              }
          } catch (Exception $e) {
              return $e->getMessage();
          }
        }
      }else{
        return redirect()->to('/');
    } 
  }

  public function get_entity_details() {
    if (isset($this->session->loged_in)){
        try {
            $entity = $this->entity_model->find($_GET['id']);
            $direction = $this->direction_model->find($entity['direction_id']);            
            $city=$this->city_model->findAll();
            $c=$this->city_model->find($direction['city_id']);
            $province=$this->province_model->findAll();
            $combinedData = [
                'entity' => $entity,
                'direction' => $direction,
                'city' => $city,
                'selected_city'=>$direction['city_id'],
                'province' => $province,
                'selected_province' =>$c['province_id']
            ];
            $json= json_encode($combinedData); // Devuelve los detalles de la entidad en formato JSON
            header('Content-Type: application/json');
            return $this->response->setJSON($json);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }else{
      return redirect()->to('/');
    }
}

public function save_entity_changes() {
  if (isset($this->session->loged_in)){
    $request = \Config\Services::request();
    $validation = \Config\Services::validation();
    $data = json_decode($request->getBody(), true);
    try {
        $entity= $this->entity_model->find($data['id']);

        $direction=[
          'street'=>$data['street'],
          'city_id'=>$data['city_id'],
          'number'=>$data['number']
        ];
        
        $new_entity=[
          'name'=>$data['name'],
          'email'=>$data['email'],
          'phone'=>$data['phone']
        ];
        $data=$direction;
        //$this->db->transStart();
        $result = $this->direction_model->update($entity['direction_id'], $direction);

        $result = $this->entity_model->update($entity['id'], $new_entity);

        $valid=[
          'email' => $new_entity['email'],
          'name' => $new_entity['name'],
          'phone'=> $new_entity['phone'],
          'city' => $direction['city_id'],
          'street'=> $direction['street'],
          'number'=> $direction['number'],
        ];

        if (!$validation->run($valid, 'modify_Entity')) {
          $errors= $validation->getErrors();
          $errors=json_encode($errors);
          $this->db->transRollback();
          return $this->response->setJSON(['success' => false, 'message' =>$errors ]);
        }
        else {
          //$this->db->transCommit();
          if ($result) {
              return $this->response->setJSON(['success' => true, 'message' => 'Los cambios se guardaron con exito']);
          } else {
              return $this->response->setJSON(['success' => false, 'message' => 'Error al guardar los cambios']);
          }
        }
    } catch (Exception $e) {
        return $this->response->setJSON(['success' => false, 'message' => 'Error al guardar los cambios: ' . $e->getMessage()]);
    }
  }else{
      return redirect()->to('/');
  } 
}

public function list_entity_with_documents_type()
{
  if (isset($this->session->loged_in)){
    $permiso = $this->rolesPermits_model->validate_permits($this->session->id_rol,'Detalle Entidad');
    if (!$permiso){
      $this->load_error_404();
    }else{
        $entity= $this->entity_model->getOne($_GET['id']);
        $consulta = $this->doc_entity->getAllbyEntity($_GET['id']);

        $this->load_views('Entity/entity_detail',[
          'consulta' => $consulta,
          'entity' =>$entity
          ] );
      } 
    }else{
      return redirect()->to('/');
    }
}

}
