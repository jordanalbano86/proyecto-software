<?php

namespace App\Controllers;

use App\Models\UserModel;

class Home extends BaseController
{
    protected $db;
    //protected \CodeIgniter\Session\Session $session;
    /**
     * @var \CodeIgniter\Model|\UserModel|null
     */

    public function __construct()
    {
    }
    public function index()
    {
        echo view('templates/header');
        if (isset($this->session->loged_in)){
            echo view('main-menu/welcome');
            echo view('templates/footer');
        }else{
            echo view('login');
         }
    }

    public function form_register(){
        echo view('templates/header');
        echo view('register');
    }

}
