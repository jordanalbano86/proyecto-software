<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */

 #Documents routes Begin ->
$routes->get('/', 'Home::index');
$routes->get('/documents/list', 'Document_controller::list');
$routes->get('/documents/delete/(:num)', 'Document_controller::delete/$1');
$routes->get('/documents/form_add', 'Document_controller::form_add');
$routes->get('/documents/form_add/entities/(:num)', 'Document_entity_controller::list_entities_by_document_type/$1');
$routes->post('/documents/new', 'Document_controller::new');
$routes->get('/documents/form_update/(:num)', 'Document_controller::form_update/$1');
$routes->post('/documents/update', 'Document_controller::update');
#<- Documents routes End
$routes->post('login', 'UserController::login');
$routes->get('logout', 'UserController::logout');
$routes->get('form_register', 'Home::form_register');
$routes->post('register', 'UserController::register');


#Entitys routes Begin ->
$routes->get('Entity_controller/entity_form', 'Entity_controller::create_entity');
$routes->post('Entity_controller/add_entity', 'Entity_controller::add_entity');
#<- Entitys routes End

#Citys routes Begin ->
$routes->get('City_controller/get_cities', 'City_controller::get_cities');
$routes->get('Entity_controller/list_entitys', 'Entity_controller::list_entitys');
$routes->delete('Entity_controller/delete_entity', 'Entity_controller::delete_entity');
$routes->get('Entity_controller/get_entity_details', 'Entity_controller::get_entity_details');
$routes->post('Entity_controller/save_entity_changes', 'Entity_controller::save_entity_changes');
#<- Entitys routes End

$routes->get('Entity_controller/list_entity_with_documents_type', 'Entity_controller::list_entity_with_documents_type');
$routes->get('document_entity/form_add', 'Document_entity_controller::form_add');
$routes->post('document_entity/add', 'Document_entity_controller::add');
$routes->delete('document_entity/delete', 'Document_entity_controller::delete');


$routes->get('document_steps/view_detail','Document_steps_controller::view_detail');
$routes->get('document_steps/form_add','Document_steps_controller::form_add');
$routes->post('document_steps/add','Document_steps_controller::add');
$routes->delete('document_steps/delete', 'Document_steps_controller::delete');
$routes->get('document_steps/form_update', 'Document_steps_controller::form_update');
$routes->post('document_steps/update', 'Document_steps_controller::update');

$routes->get('document_types/list', 'Document_types_controller::list');
$routes->get('document_types/form_add', 'Document_types_controller::form_add');
$routes->get('document_types/form_update/(:num)', 'Document_types_controller::form_update/$1');
$routes->post('document_types/update', 'Document_types_controller::update');
$routes->post('document_types/delete', 'Document_types_controller::delete');
$routes->post('document_types/add', 'Document_types_controller::add');

$routes->get('users/list', 'UserController::list');
$routes->post('users/register', 'UserController::register');
$routes->get('users/form_register', 'UserController::form_register');
$routes->post('users/form_update', 'UserController::form_update');
$routes->post('users/update', 'UserController::update');
$routes->post('users/update_citizen', 'UserController::update_citizen');
$routes->post('users/update_password', 'UserController::update_password');
$routes->post('users/delete', 'UserController::delete');
$routes->get('/users/find/(:id)', 'UserController::find/$1');
$routes->get('users/profile', 'UserController::load_profile');

$routes->get('roles/list', 'Roles_controller::list');
$routes->get('roles/form_add', 'Roles_controller::form_add');
$routes->get('roles/form_update/(:num)', 'Roles_controller::form_update/$1');
$routes->post('roles/update', 'Roles_controller::update');
$routes->post('roles/add', 'Roles_controller::add');
$routes->post('roles/delete', 'Roles_controller::delete');
$routes->post('roles/list_for_rol', 'Roles_controller::list_for_rol');
$routes->post('roles/add_permits', 'Roles_controller::add_permits');
$routes->post('users/login', 'UserController::login');
$routes->get('users/logout', 'UserController::logout');
$routes->get('roles/form_permits_add', 'Roles_controller::form_permits_add');
$routes->post('roles/new_permits', 'Roles_controller::new_permits');

$routes->get('complain/form_add', 'Complains_controller::form_complain');
$routes->post('complain/add_complain', 'Complains_controller::add_complain');
$routes->get('complain/list', 'Complains_controller::list_complains');
$routes->get('complain/list_admin', 'Complains_controller::list_complains_admin');
$routes->get('complain/view_detail', 'Complains_controller::view_detail');
$routes->get('document_steps/get_steps_by_document_entity', 'Document_steps_controller::get_steps_by_document_entity');

$routes->post('complain/init_complain', 'Complains_controller::init_complain');
$routes->post('complain/term_complain', 'Complains_controller::term_complain');



$routes->get('notifications/users', 'Notification_controller::getNotificationOfUser');
$routes->get('notifications/users/count', 'Notification_controller::getAmountOfNotificationOfUser');
$routes->post('notifications/read', 'Notification_controller::read');
