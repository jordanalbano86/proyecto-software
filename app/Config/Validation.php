<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;
use CodeIgniter\Validation\StrictRules\CreditCardRules;
use CodeIgniter\Validation\StrictRules\FileRules;
use CodeIgniter\Validation\StrictRules\FormatRules;
use CodeIgniter\Validation\StrictRules\Rules;
use App\Libraries\validation\Document_rules;

class Validation extends BaseConfig
{
    // --------------------------------------------------------------------
    // Setup
    // --------------------------------------------------------------------

    /**
     * Stores the classes that contain the
     * rules that are available.
     *
     * @var string[]
     */
    public array $ruleSets = [
        Rules::class,
        FormatRules::class,
        FileRules::class,
        CreditCardRules::class,
        Document_Rules::class
    ];

    /**
     * Specifies the views that are used to display the
     * errors.
     *
     * @var array<string, string>
     */
    public array $templates = [
        'list'   => 'CodeIgniter\Validation\Views\list',
        'single' => 'CodeIgniter\Validation\Views\single',
    ];

    public array $modify_Entity = [
      'email' => 'required|max_length[254]|valid_email',
      'name'  => 'required',
      'phone'  => 'required|decimal',
      'city'  => 'required',
      'street'  => 'required|max_length[254]',
      'number'  => 'required|max_length[254]|decimal',
   ];
   public array $modify_Entity_errors = [
       'email' => [
           'required' => 'El campo email no puede estar vacio.',
           'valid_email' => 'El email parace no ser un email valido'
       ],
       'name' => [
           'required' => 'El nombre no puede estar vacio.'
       ],
       'phone' =>[
         'required' => 'El telefono no puede estar vacio',
         'decimal' => 'El numero de telefono solo puede contener numeros'
       ],
       'city' =>[
         'required' => 'Por favor selecione una ciudad'
       ],

       'street' =>[
         'required' => 'Por favor coloque una calle'
       ],
       'number' =>[
         'required' => 'El numero de la calle no puede estar vacio',
         'decimal' => 'El numero numero de la calle solo puede contener numeros'
       ]
   ];


    // --------------------------------------------------------------------
    // Rules
    // --------------------------------------------------------------------
}
