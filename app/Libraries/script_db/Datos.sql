SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

use billeteradb;

INSERT INTO `province` (`id`, `name`) VALUES
(4, 'Buenos Aires'),
(15, 'Catamarca'),
(19, 'Chaco'),
(7, 'Chubut'),
(11, 'Cordoba'),
(21, 'Corrientes'),
(23, 'Entre Rios'),
(20, 'Formosa'),
(17, 'Jujuy'),
(5, 'La Pampa'),
(13, 'La Rioja'),
(12, 'Mendoza'),
(22, 'Misiones'),
(6, 'Neuquen'),
(3, 'Rio Negro'),
(16, 'Salta'),
(10, 'San Juan'),
(14, 'San Luis'),
(8, 'Santa Cruz'),
(24, 'Santa Fe'),
(25, 'Santiago del Estero'),
(9, 'Tierra del Fuego'),
(18, 'Tucuman');

INSERT INTO `city` (`id`, `name`, `province_id`) VALUES
(26, 'Antofagasta de la Sierra', 15),
(28, 'Burruyacu', 18),
(40, 'Candelaria', 22),
(38, 'Clorinda', 20),
(42, 'Concepcion', 21),
(46, 'Concordia', 23),
(21, 'Cordoba', 11),
(41, 'Corrientes', 21),
(37, 'Formosa', 20),
(3, 'La Plata', 4),
(32, 'La Quiaca', 17),
(23, 'La Rioja', 13),
(10, 'Los Antiguos', 8),
(6, 'Macachin', 5),
(15, 'Mendoza', 12),
(20, 'Mercedes', 14),
(13, 'Neuquen', 6),
(45, 'Parana', 23),
(4, 'Patagones', 4),
(39, 'Posadas', 22),
(12, 'Puerto Madryn', 7),
(11, 'Rawson', 7),
(35, 'Resistencia', 19),
(22, 'Rio Cuarto', 11),
(9, 'Rio Gallegos', 8),
(8, 'Rio Grande', 9),
(30, 'Rivadavia', 16),
(2, 'Roca', 3),
(36, 'Roque Saenz Peña', 19),
(44, 'Rosario', 24),
(29, 'Salta', 16),
(25, 'San Fernando del V. de Catamarca', 15),
(17, 'San Juan', 10),
(19, 'San Luis', 14),
(27, 'San Miguel de Tucuman', 18),
(16, 'San Rafael', 12),
(31, 'San Salvador de Jujuy', 17),
(43, 'Santa Fe', 24),
(5, 'Santa Rosa', 5),
(33, 'Santiago del Estero', 25),
(18, 'Talacasto', 10),
(34, 'Termas de Rio Hondo', 25),
(7, 'Usuahia', 9),
(1, 'Viedma', 3),
(24, 'Villa Union', 13),
(14, 'Zapala', 6);

INSERT INTO `status` (`id`, `name`) VALUES
(4, 'CANCELADA'),
(2, 'EN PROCESO'),
(3, 'FINALIZADA'),
(1, 'INICIADA');

INSERT INTO `complaints` (`id`, `start_date`, `end_date`, `modification_date`, `status_id`) VALUES
(20, '2023-11-22', '2023-11-22', '2023-11-22', 2),
(21, '2023-11-22', NULL, '2023-11-22', 1);

--
-- Volcado de datos para la tabla `direction`
--

INSERT INTO `direction` (`id`, `city_id`, `street`, `number`) VALUES
(29, 1, 'Saavedra', 1144),
(31, 1, 'San Martin', 320),
(32, 15, 'Cabildo', 1345),
(33, 3, 'Alen', 1367),
(34, 21, 'Av. Rafael Nuñez', 4208),
(39, 4, 'San Martin', 14);

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'ADMINISTRADOR'),
(2, 'CIUDADANO');


INSERT INTO `users` (`id`, `username`, `email`, `name`, `lastname`, `password`, `role_id`) VALUES
(26, 'ltomasini', 'ltomasini@unrn.edu.ar', 'Laura', 'Tomasini', '$2y$10$ffR62/8.AOtOk9CMGlet/urUm9v7LFbqcpUC6IkzGOa.k9iWzyZvO', 2),
(1, 'admin', 'admin@unrn.edu.ar', 'administrador', 'admin', '$2y$10$MmlGmV6humlRRO3CQave1.aYb/Wv0Pb5Ust2Z0LPxMPVYlL5qjP1i', 1);



INSERT INTO `document_types` (`id`, `name`) VALUES
(1, 'Tarjeta Visa Credito'),
(2, 'Tarjeta Visa Debito'),
(10, 'Licencia Nacional de Conducir'),
(11, 'DNI'),
(14, 'Tarjeta de credito mastercard');


INSERT INTO `entity` (`id`, `name`, `email`, `direction_id`, `phone`) VALUES
(32, 'Banco de la Nacion Argentina', 'bna@bna.com.ar', 31, 454545),
(33, 'RENAPER', 'renaper@renaper.gob.ar', 32, 456784),
(34, 'Centro de Licencias de Conducir', 'clnc@lnc.gob.ar', 33, 43589212),
(35, 'Banco Santander Rio', 'bsrio@santander.com.ar', 34, 46789021),
(40, 'Banco Provincia', 'bprovincia@gmail.com', 39, 123);

INSERT INTO `document_entity` (`id`, `entity_id`, `document_type_id`) VALUES
(3, 32, 1),
(4, 33, 11),
(5, 34, 10),
(6, 35, 2),
(14, 40, 1),
(15, 40, 2);




INSERT INTO `documents` (`id`, `date_of_issue`, `expiration_date`, `number`, `document_entity_id`, `user_id`, `activated`) VALUES
(35, '2017-11-22', '2023-11-22', '045412', 3, 26, 0),
(36, '2017-11-22', '2023-11-30', '45214144', 3, 26, 0),
(37, '2023-11-01', '2023-11-30', '652145632146', 3, 26, 1);

--
-- Volcado de datos para la tabla `documents_complains`
--

INSERT INTO `documents_complains` (`id`, `complain_id`, `document_id`) VALUES
(19, 20, 36),
(20, 21, 35);



INSERT INTO `notifications` (`id`, `message`, `transmitter`, `receiver`, `read`, `created_on`, `created_at`, `updated_at`) VALUES
(4, 'Se ha generado una nueva denuncia', 26, 1, 1, '2023-11-22', '0000-00-00', '0000-00-00'),
(5, 'Se ha generado una nueva denuncia', 26, 1, 1, '2023-11-22', '0000-00-00', '0000-00-00'),
(6, 'Tu documentacion cambio de estado', 1, 26, 1, '2023-11-22', '0000-00-00', '0000-00-00');

--
-- Volcado de datos para la tabla `permits`
--

INSERT INTO `permits` (`id`, `name`, `lavel`) VALUES
(1, 'Gestion Tipos de Documento', 1),
(2, 'Gestion Entidades', 1),
(3, 'Gestion Usuarios', 1),
(4, 'Gestion Roles', 1),
(5, 'Listado Tipos Documento', 2),
(6, 'Listado Entidades', 2),
(7, 'Agregar Entidad', 2),
(8, 'Listado Usuarios', 2),
(9, 'Registrar Usuario', 2),
(10, 'Agregar Tipo Documento', 3),
(11, 'Modificar Tipo Documento', 3),
(12, 'Eliminar Tipo Documento', 3),
(13, 'Modificar Entidad', 3),
(14, 'Eliminar Entidad', 3),
(15, 'Detalle Entidad', 3),
(16, 'Modificar Usuario', 3),
(17, 'Eliminar Usuario', 3),
(18, 'Detalle Usuario', 3),
(19, 'Permisos Rol', 3),
(20, 'Modificar Rol', 3),
(21, 'Eliminar Rol', 3),
(22, 'Agregar Rol', 3),
(45, 'Modificar Paso Recuperacion', 3),
(46, 'Eliminar Paso Recuperacion', 3),
(47, 'Detalle Paso Recuperacion', 3),
(48, 'Agregar Tipo Documento a Entidad', 3),
(49, 'Quitar Tipo Documento a Entidad', 3),
(50, 'Perfil Ciudadano', 2),
(51, 'Listado Mi Documentacion', 2),
(52, 'Gestion Mi Billetera', 1),
(53, 'Agregar Documentacion', 2),
(54, 'Modificar Documentacion', 2),
(55, 'Eliminar Documentacion', 3),
(56, 'Nuevo Permiso', 2),
(57, 'Listado Denuncia Admin', 2),
(58, 'Listado Denuncia Ciudadano', 2),
(59, 'Agregar Denuncia', 3),
(60, 'Ver Detalle de Denuncia', 3),
(61, 'Administrar Denuncia', 3);



INSERT INTO `recuperations_steps` (`id`, `document_entity_id`, `number`, `description`) VALUES
(1, 6, 1, 'Comunicarse al (11) 4325 4332'),
(2, 6, 2, 'Realizar la Denuncia'),
(3, 6, 3, 'Se reimprimirá una nueva tarjeta'),
(4, 6, 4, 'La tarjeta llegara a su domicilio en el transcurso de 10 dias'),
(5, 3, 1, 'Comunicarse al 0800-222-2323'),
(6, 3, 2, 'Denunciar el extravio de la tarjeta'),
(7, 3, 3, 'Se reimprimirá una nueva tarjeta'),
(8, 3, 4, 'La tarjeta llegara a su domicilio en el transcurso de 10 dias'),
(9, 4, 1, 'Sacar turno en mi.argentina.gob.ar/turnos'),
(10, 4, 2, 'Debera elegir un Centro de Documentacion Renaper cercano a su domicilio'),
(11, 4, 3, 'Presentarse en la oficina elegida con la constanccia del turno'),
(12, 4, 4, 'Guardar la constancia de solicitud del tramite'),
(13, 4, 5, 'Presentar la constancia de solicitud al correo para recibirlo'),
(14, 5, 1, 'Resolve tus infracciones de transito de la ciudad'),
(15, 5, 2, 'Consulta tus infracciones en laplata.gob.ar/infracciones-de-transito'),
(16, 5, 3, 'Charla de renovacion online'),
(17, 5, 4, 'Abonar el Certificado Nacional de Antecedentes de Transito (CENAT)'),
(18, 5, 5, 'Sacar un torno para el tramite'),
(19, 5, 6, 'Abonar la boleta unica inteligente'),
(20, 5, 7, 'Dirigirse a la sede elegida para realizar los examenes psicofisicos'),
(21, 5, 8, 'Retira la licencia en la sede'),
(25, 14, 1, 'Sacar turno');



INSERT INTO `roles_permits` (`id`, `roles_id`, `permits_id`) VALUES
(529, 1, 1),
(530, 1, 2),
(531, 1, 3),
(532, 1, 4),
(533, 1, 5),
(534, 1, 6),
(535, 1, 7),
(536, 1, 8),
(537, 1, 9),
(538, 1, 10),
(539, 1, 11),
(540, 1, 12),
(541, 1, 13),
(542, 1, 14),
(543, 1, 15),
(544, 1, 16),
(545, 1, 17),
(546, 1, 18),
(547, 1, 19),
(548, 1, 20),
(549, 1, 21),
(550, 1, 22),
(551, 1, 45),
(552, 1, 46),
(553, 1, 47),
(554, 1, 48),
(555, 1, 49),
(556, 1, 50),
(557, 1, 51),
(558, 1, 52),
(559, 1, 53),
(560, 1, 54),
(561, 1, 55),
(562, 1, 56),
(563, 1, 57),
(564, 1, 58),
(565, 1, 59),
(566, 1, 60),
(567, 1, 61),
(568, 2, 5),
(569, 2, 6),
(570, 2, 16),
(571, 2, 47),
(572, 2, 50),
(573, 2, 51),
(574, 2, 52),
(575, 2, 53),
(576, 2, 54),
(577, 2, 55),
(578, 2, 58),
(579, 2, 59),
(580, 2, 60);


COMMIT;
