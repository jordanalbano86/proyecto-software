<?php

namespace Config;

use CodeIgniter\Config\Validation;


class entity_validation extends Validation
{
    // --------------------------------------------------------------------
    // Setup
    // --------------------------------------------------------------------

    /**
     * Stores the classes that contain the
     * rules that are available.
     *
     * @var string[]
     */

    /**
     * Specifies the views that are used to display the
     * errors.
     *
     * @var array<string, string>
     */

    public array $modify_Entity = [
      'email' => 'required|max_length[254]|valid_email',
      'name'  => 'required',
      'phone'  => 'required|decimal',
      'city'  => 'required',
      'street'  => 'required|max_length[254]',
      'number'  => 'required|max_length[254]|decimal',
   ];
   public array $modify_Entity_errors = [
       'email' => [
           'required' => 'El campo email no puede estar vacio.',
           'valid_email' => 'El email parace no ser un email valido'
       ],
       'name' => [
           'required' => 'El nombre no puede estar vacio.'
       ],
       'phone' =>[
         'required' => 'El telefono no puede estar vacio',
         'decimal' => 'El numero de telefono solo puede contener numeros'
       ],
       'city' =>[
         'required' => 'Por favor selecione una ciudad'
       ],

       'street' =>[
         'required' => 'Por favor coloque una calle'
       ],
       'number' =>[
         'required' => 'El numero de la calle no puede estar vacio',
         'decimal' => 'El numero numero de la calle solo puede contener numeros'
       ]
   ];


    // --------------------------------------------------------------------
    // Rules
    // --------------------------------------------------------------------
}
