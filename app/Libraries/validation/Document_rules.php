<?php
namespace app\Libraries\validation;
use App\Models\Document_model;
class Document_rules
{
    public function isLater(string $str, string $field, array $data): bool
    {
        $dateOfIssue = $data['date_of_issue'] ?? null;
        $expirationDate = $data['expiration_date'] ?? null;


        if (!empty($expirationDate)) {
            // Convierte las fechas a timestamps para comparar
            $timestampPosterior = strtotime($str);
            $timestampAnterior = strtotime($dateOfIssue);

            // Compara los timestamps
            return $timestampPosterior > $timestampAnterior;
        }

        return true;
    }


    
}